package co.com.bogota.planeaciondistrital.model.planAccion;

import java.util.Date;

public class IndicadorModel {
    private Integer TIPO_CODIGO;
    private String NOMBRE_INDICADOR;
    private String FORMULA_INDICADOR;
    private String INDICADOR_PDD;
    private Date TIEMPO_EJECUCION_INICIO;
    private Date TIEMPO_EJECUCION_FIN;
    private String LINEA_BASE_VALOR;
    private Date LINEA_BASE_FECHA;
    private String LINEA_BASE_FUENTE;
    private String CODIGO_RESULTADO;
    private String UNIDAD_MEDIDA;
    private String TIPO_ANUALIZACION;
    private String CODIGO_INDICADOR_PDD;
    private String USUARIO;
    private String MENSAJE;
    private Integer VALIDACION;
    private String CODIGO_INDICADOR;

    public IndicadorModel() {
    }

    public IndicadorModel(String NOMBRE_INDICADOR, String FORMULA_INDICADOR, String INDICADOR_PDD, Date TIEMPO_EJECUCION_INICIO, Date TIEMPO_EJECUCION_FIN, String LINEA_BASE_VALOR, Date LINEA_BASE_FECHA, String LINEA_BASE_FUENTE, String CODIGO_RESULTADO, String UNIDAD_MEDIDA, String TIPO_ANUALIZACION, String CODIGO_INDICADOR_PDD, String USUARIO, String MENSAJE, Integer VALIDACION, String CODIGO_INDICADOR) {
        this.NOMBRE_INDICADOR = NOMBRE_INDICADOR;
        this.FORMULA_INDICADOR = FORMULA_INDICADOR;
        this.INDICADOR_PDD = INDICADOR_PDD;
        this.TIEMPO_EJECUCION_INICIO = TIEMPO_EJECUCION_INICIO;
        this.TIEMPO_EJECUCION_FIN = TIEMPO_EJECUCION_FIN;
        this.LINEA_BASE_VALOR = LINEA_BASE_VALOR;
        this.LINEA_BASE_FECHA = LINEA_BASE_FECHA;
        this.LINEA_BASE_FUENTE = LINEA_BASE_FUENTE;
        this.CODIGO_RESULTADO = CODIGO_RESULTADO;
        this.UNIDAD_MEDIDA = UNIDAD_MEDIDA;
        this.TIPO_ANUALIZACION = TIPO_ANUALIZACION;
        this.CODIGO_INDICADOR_PDD = CODIGO_INDICADOR_PDD;
        this.USUARIO = USUARIO;
        this.MENSAJE = MENSAJE;
        this.VALIDACION = VALIDACION;
        this.CODIGO_INDICADOR = CODIGO_INDICADOR;
    }

    public String getNOMBRE_INDICADOR() {
        return NOMBRE_INDICADOR;
    }

    public void setNOMBRE_INDICADOR(String NOMBRE_INDICADOR) {
        this.NOMBRE_INDICADOR = NOMBRE_INDICADOR;
    }

    public String getFORMULA_INDICADOR() {
        return FORMULA_INDICADOR;
    }

    public void setFORMULA_INDICADOR(String FORMULA_INDICADOR) {
        this.FORMULA_INDICADOR = FORMULA_INDICADOR;
    }

    public String getINDICADOR_PDD() {
        return INDICADOR_PDD;
    }

    public void setINDICADOR_PDD(String INDICADOR_PDD) {
        this.INDICADOR_PDD = INDICADOR_PDD;
    }

    public Date getTIEMPO_EJECUCION_INICIO() {
        return TIEMPO_EJECUCION_INICIO;
    }

    public void setTIEMPO_EJECUCION_INICIO(Date TIEMPO_EJECUCION_INICIO) {
        this.TIEMPO_EJECUCION_INICIO = TIEMPO_EJECUCION_INICIO;
    }

    public Date getTIEMPO_EJECUCION_FIN() {
        return TIEMPO_EJECUCION_FIN;
    }

    public void setTIEMPO_EJECUCION_FIN(Date TIEMPO_EJECUCION_FIN) {
        this.TIEMPO_EJECUCION_FIN = TIEMPO_EJECUCION_FIN;
    }

    public String getLINEA_BASE_VALOR() {
        return LINEA_BASE_VALOR;
    }

    public void setLINEA_BASE_VALOR(String LINEA_BASE_VALOR) {
        this.LINEA_BASE_VALOR = LINEA_BASE_VALOR;
    }

    public Date getLINEA_BASE_FECHA() {
        return LINEA_BASE_FECHA;
    }

    public void setLINEA_BASE_FECHA(Date LINEA_BASE_FECHA) {
        this.LINEA_BASE_FECHA = LINEA_BASE_FECHA;
    }

    public String getLINEA_BASE_FUENTE() {
        return LINEA_BASE_FUENTE;
    }

    public void setLINEA_BASE_FUENTE(String LINEA_BASE_FUENTE) {
        this.LINEA_BASE_FUENTE = LINEA_BASE_FUENTE;
    }

    public String getCODIGO_RESULTADO() {
        return CODIGO_RESULTADO;
    }

    public void setCODIGO_RESULTADO(String CODIGO_RESULTADO) {
        this.CODIGO_RESULTADO = CODIGO_RESULTADO;
    }

    public String getUNIDAD_MEDIDA() {
        return UNIDAD_MEDIDA;
    }

    public void setUNIDAD_MEDIDA(String UNIDAD_MEDIDA) {
        this.UNIDAD_MEDIDA = UNIDAD_MEDIDA;
    }

    public String getTIPO_ANUALIZACION() {
        return TIPO_ANUALIZACION;
    }

    public void setTIPO_ANUALIZACION(String TIPO_ANUALIZACION) {
        this.TIPO_ANUALIZACION = TIPO_ANUALIZACION;
    }

    public String getCODIGO_INDICADOR_PDD() {
        return CODIGO_INDICADOR_PDD;
    }

    public void setCODIGO_INDICADOR_PDD(String CODIGO_INDICADOR_PDD) {
        this.CODIGO_INDICADOR_PDD = CODIGO_INDICADOR_PDD;
    }

    public String getUSUARIO() {
        return USUARIO;
    }

    public void setUSUARIO(String USUARIO) {
        this.USUARIO = USUARIO;
    }

    public String getMENSAJE() {
        return MENSAJE;
    }

    public void setMENSAJE(String MENSAJE) {
        this.MENSAJE = MENSAJE;
    }

    public Integer getVALIDACION() {
        return VALIDACION;
    }

    public void setVALIDACION(Integer VALIDACION) {
        this.VALIDACION = VALIDACION;
    }

    public String getCODIGO_INDICADOR() {
        return CODIGO_INDICADOR;
    }

    public void setCODIGO_INDICADOR(String CODIGO_INDICADOR) {
        this.CODIGO_INDICADOR = CODIGO_INDICADOR;
    }

    public Integer getTIPO_CODIGO() {
        return TIPO_CODIGO;
    }

    public void setTIPO_CODIGO(Integer TIPO_CODIGO) {
        this.TIPO_CODIGO = TIPO_CODIGO;
    }

    @Override
    public String toString() {
        return "IndicadorModel{" +
                "TIPO_CODIGO='" + TIPO_CODIGO + '\'' +
                "NOMBRE_INDICADOR='" + NOMBRE_INDICADOR + '\'' +
                ", FORMULA_INDICADOR='" + FORMULA_INDICADOR + '\'' +
                ", INDICADOR_PDD='" + INDICADOR_PDD + '\'' +
                ", TIEMPO_EJECUCION_INICIO=" + TIEMPO_EJECUCION_INICIO +
                ", TIEMPO_EJECUCION_FIN=" + TIEMPO_EJECUCION_FIN +
                ", LINEA_BASE_VALOR='" + LINEA_BASE_VALOR + '\'' +
                ", LINEA_BASE_FECHA=" + LINEA_BASE_FECHA +
                ", LINEA_BASE_FUENTE='" + LINEA_BASE_FUENTE + '\'' +
                ", CODIGO_RESULTADO='" + CODIGO_RESULTADO + '\'' +
                ", UNIDAD_MEDIDA='" + UNIDAD_MEDIDA + '\'' +
                ", TIPO_ANUALIZACION='" + TIPO_ANUALIZACION + '\'' +
                ", CODIGO_INDICADOR_PDD='" + CODIGO_INDICADOR_PDD + '\'' +
                ", USUARIO='" + USUARIO + '\'' +
                ", MENSAJE='" + MENSAJE + '\'' +
                ", VALIDACION=" + VALIDACION +
                ", VALIDACION=" + CODIGO_INDICADOR +
                '}';
    }

}
