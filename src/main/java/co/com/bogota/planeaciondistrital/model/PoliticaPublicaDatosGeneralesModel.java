package co.com.bogota.planeaciondistrital.model;

import java.util.Date;

/**
 * 
 * @author Daniel Arbey Sarmiento Amaya
 * @see contiene la informacion que será enviada o recibida de la Base de datos
 */
public class PoliticaPublicaDatosGeneralesModel {

	private String nombrePolitica;
	private Integer nemotDocumento;
	private Integer anno;
	private Date fechaInicio;
	private Date fechaFin;
	private String sectorLider;
	private String entidadLider;
	private String costoTotalPolitica;
	private Date fechaAprobacion;
	private String sectoresCorresponsales[];
	public String getNombrePolitica() {
		return nombrePolitica;
	}
	public void setNombrePolitica(String nombrePolitica) {
		this.nombrePolitica = nombrePolitica;
	}
	public Integer getNemotDocumento() {
		return nemotDocumento;
	}
	public void setNemotDocumento(Integer nemotDocumento) {
		this.nemotDocumento = nemotDocumento;
	}
	public Integer getAnno() {
		return anno;
	}
	public void setAnno(Integer anno) {
		this.anno = anno;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getSectorLider() {
		return sectorLider;
	}
	public void setSectorLider(String sectorLider) {
		this.sectorLider = sectorLider;
	}
	public String getEntidadLider() {
		return entidadLider;
	}
	public void setEntidadLider(String entidadLider) {
		this.entidadLider = entidadLider;
	}
	public String getCostoTotalPolitica() {
		return costoTotalPolitica;
	}
	public void setCostoTotalPolitica(String costoTotalPolitica) {
		this.costoTotalPolitica = costoTotalPolitica;
	}
	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}
	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	public String[] getSectoresCorresponsales() {
		return sectoresCorresponsales;
	}
	public void setSectoresCorresponsales(String[] sectoresCorresponsales) {
		this.sectoresCorresponsales = sectoresCorresponsales;
	}
	
	
}
