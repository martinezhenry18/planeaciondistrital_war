package co.com.bogota.planeaciondistrital.model.planAccion;

public class EntidadesModel {

    private String NOMBRE;
    private String VALOR;

    public EntidadesModel() {
    }

    public EntidadesModel(String NOMBRE, String VALOR) {
        this.NOMBRE = NOMBRE;
        this.VALOR = VALOR;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public String getVALOR() {
        return VALOR;
    }

    public void setVALOR(String VALOR) {
        this.VALOR = VALOR;
    }

    @Override
    public String toString() {
        return "ListaEntidadesModel{" +
                "NOMBRE='" + NOMBRE + '\'' +
                ", VALOR='" + VALOR + '\'' +
                '}';
    }
}
