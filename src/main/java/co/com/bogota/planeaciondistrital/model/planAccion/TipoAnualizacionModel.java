package co.com.bogota.planeaciondistrital.model.planAccion;

public class TipoAnualizacionModel {

    private String NOMBRE;
    private String CODIGO_PARAMETROS_SISTEMA;

    public TipoAnualizacionModel() {
    }

    public TipoAnualizacionModel(String NOMBRE, String CODIGO_PARAMETROS_SISTEMA) {
        this.NOMBRE = NOMBRE;
        this.CODIGO_PARAMETROS_SISTEMA = CODIGO_PARAMETROS_SISTEMA;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public String getCODIGO_PARAMETROS_SISTEMA() {
        return CODIGO_PARAMETROS_SISTEMA;
    }

    public void setCODIGO_PARAMETROS_SISTEMA(String CODIGO_PARAMETROS_SISTEMA) {
        this.CODIGO_PARAMETROS_SISTEMA = CODIGO_PARAMETROS_SISTEMA;
    }

    @Override
    public String toString() {
        return "TipoAnualizacionModel{" +
                "NOMBRE='" + NOMBRE + '\'' +
                ", CODIGO_PARAMETROS_SISTEMA='" + CODIGO_PARAMETROS_SISTEMA + '\'' +
                '}';
    }
}
