package co.com.bogota.planeaciondistrital.model.parametrizacion;

/**
	*	@author 	Henry Martinez
	*	@empresa	Ares Soluciones S.A.S
	*	@mail		hmartinez@aressoluciones.com
	*	@fecha 		21/05/2019
	*	@proyeco	Planeacion Distrital
	*	@clase 		ParametrizacionListasBasicasModel.java
	*	@comments	
 */

public class ParametrizacionListasBasicasModel
{
	
	private Integer idLista;
	private String nombreLista;
	private String descripcionLista = null;
	private String estadoLista;
	private String valorLista = null;
	private Integer tipoParametroLista = null;
	
	
	public Integer getIdLista()
	{
		return idLista;
	}
	public void setIdLista(Integer idLista)
	{
		this.idLista = idLista;
	}
	public String getNombreLista()
	{
		return nombreLista;
	}
	public void setNombreLista(String nombreLista)
	{
		this.nombreLista = nombreLista;
	}
	public String getEstadoLista()
	{
		return estadoLista;
	}
	public void setEstadoLista(String estadoLista)
	{
		this.estadoLista = estadoLista;
	}
	public String getDescripcionLista()
	{
		return descripcionLista;
	}
	public void setDescripcionLista(String descripcionLista)
	{
		this.descripcionLista = descripcionLista;
	}
	public String getValorLista()
	{
		return valorLista;
	}
	public void setValorLista(String valorLista)
	{
		this.valorLista = valorLista;
	}
	public Integer getTipoParametroLista()
	{
		return tipoParametroLista;
	}
	public void setTipoParametroLista(Integer tipoParametroLista)
	{
		this.tipoParametroLista = tipoParametroLista;
	}
	
	
	
}
