package co.com.bogota.planeaciondistrital.model.planAccion;

public class UnidadesMedidasModel {
    private String NOMBRE;
    private String VALOR;

    public UnidadesMedidasModel() {
    }

    public UnidadesMedidasModel(String NOMBRE, String VALOR) {
        this.NOMBRE = NOMBRE;
        this.VALOR = VALOR;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public String getVALOR() {
        return VALOR;
    }

    public void setVALOR(String VALOR) {
        this.VALOR = VALOR;
    }

    @Override
    public String toString() {
        return "ListaUnidadesMedidasModel{" +
                "NOMBRE='" + NOMBRE + '\'' +
                ", VALOR='" + VALOR + '\'' +
                '}';
    }


}
