package co.com.bogota.planeaciondistrital.model.planAccion;

import java.util.Objects;

public class respuestaConsultaObjetivosModel {

    private String CODIGO_OBJETIVO;
    private String OBJETIVO_GENERAL;
    private String CODIGO_OBJETIVO_ESPECIFICO;
    private String OBJETIVO_ESPECIFICO;

    public respuestaConsultaObjetivosModel(String CODIGO_OBJETIVO, String OBJETIVO_GENERAL, String CODIGO_OBJETIVO_ESPECIFICO, String OBJETIVO_ESPECIFICO) {
        this.CODIGO_OBJETIVO = CODIGO_OBJETIVO;
        this.OBJETIVO_GENERAL = OBJETIVO_GENERAL;
        this.CODIGO_OBJETIVO_ESPECIFICO = CODIGO_OBJETIVO_ESPECIFICO;
        this.OBJETIVO_ESPECIFICO = OBJETIVO_ESPECIFICO;
    }

    public respuestaConsultaObjetivosModel() {
    }

    public String getCODIGO_OBJETIVO() {
        return CODIGO_OBJETIVO;
    }

    public void setCODIGO_OBJETIVO(String CODIGO_OBJETIVO) {
        this.CODIGO_OBJETIVO = CODIGO_OBJETIVO;
    }

    public String getOBJETIVO_GENERAL() {
        return OBJETIVO_GENERAL;
    }

    public void setOBJETIVO_GENERAL(String OBJETIVO_GENERAL) {
        this.OBJETIVO_GENERAL = OBJETIVO_GENERAL;
    }

    public String getCODIGO_OBJETIVO_ESPECIFICO() {
        return CODIGO_OBJETIVO_ESPECIFICO;
    }

    public void setCODIGO_OBJETIVO_ESPECIFICO(String CODIGO_OBJETIVO_ESPECIFICO) {
        this.CODIGO_OBJETIVO_ESPECIFICO = CODIGO_OBJETIVO_ESPECIFICO;
    }

    public String getOBJETIVO_ESPECIFICO() {
        return OBJETIVO_ESPECIFICO;
    }

    public void setOBJETIVO_ESPECIFICO(String OBJETIVO_ESPECIFICO) {
        this.OBJETIVO_ESPECIFICO = OBJETIVO_ESPECIFICO;
    }

    @Override
    public String toString() {
        return "respuestaConsultaObjetivosModel{" +
                "CODIGO_OBJETIVO='" + CODIGO_OBJETIVO + '\'' +
                ", OBJETIVO_GENERAL='" + OBJETIVO_GENERAL + '\'' +
                ", CODIGO_OBJETIVO_ESPECIFICO='" + CODIGO_OBJETIVO_ESPECIFICO + '\'' +
                ", OBJETIVO_ESPECIFICO='" + OBJETIVO_ESPECIFICO + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof respuestaConsultaObjetivosModel)) return false;
        respuestaConsultaObjetivosModel that = (respuestaConsultaObjetivosModel) o;
        return getCODIGO_OBJETIVO().equals(that.getCODIGO_OBJETIVO()) &&
                getOBJETIVO_GENERAL().equals(that.getOBJETIVO_GENERAL()) &&
                getCODIGO_OBJETIVO_ESPECIFICO().equals(that.getCODIGO_OBJETIVO_ESPECIFICO()) &&
                getOBJETIVO_ESPECIFICO().equals(that.getOBJETIVO_ESPECIFICO());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCODIGO_OBJETIVO(), getOBJETIVO_GENERAL(), getCODIGO_OBJETIVO_ESPECIFICO(), getOBJETIVO_ESPECIFICO());
    }
}
