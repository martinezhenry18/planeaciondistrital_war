package co.com.bogota.planeaciondistrital.model.planAccion;

public class ConsultaAniosModel {

    Integer ANIO;
    Integer VALOR;

    public ConsultaAniosModel() {
    }

    public ConsultaAniosModel(Integer ANIO, Integer VALOR) {
        this.ANIO = ANIO;
        this.VALOR = VALOR;
    }

    public Integer getANIO() {
        return ANIO;
    }

    public void setANIO(Integer ANIO) {
        this.ANIO = ANIO;
    }

    public Integer getVALOR() {
        return VALOR;
    }

    public void setVALOR(Integer VALOR) {
        this.VALOR = VALOR;
    }

    @Override
    public String toString() {
        return "ConsultaAniosModel{" +
                "ANIO=" + ANIO +
                ", VALOR=" + VALOR +
                '}';
    }
}
