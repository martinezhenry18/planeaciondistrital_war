package co.com.bogota.planeaciondistrital.dto.planAccion;

import java.io.Serializable;

public class TipoAnualizacionDto implements Serializable {

    private String NOMBRE;
    private String CODIGO_PARAMETROS_SISTEMA;

    public TipoAnualizacionDto() {
    }

    public TipoAnualizacionDto(String NOMBRE, String CODIGO_PARAMETROS_SISTEMA) {
        this.NOMBRE = NOMBRE;
        this.CODIGO_PARAMETROS_SISTEMA = CODIGO_PARAMETROS_SISTEMA;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public String getCODIGO_PARAMETROS_SISTEMA() {
        return CODIGO_PARAMETROS_SISTEMA;
    }

    public void setCODIGO_PARAMETROS_SISTEMA(String CODIGO_PARAMETROS_SISTEMA) {
        this.CODIGO_PARAMETROS_SISTEMA = CODIGO_PARAMETROS_SISTEMA;
    }

}
