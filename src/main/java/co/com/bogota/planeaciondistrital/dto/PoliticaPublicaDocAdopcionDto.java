package co.com.bogota.planeaciondistrital.dto;

import java.io.Serializable;

/**
 * 
 * @author Daniel Sarmiento Amaya
 * @see Contiene la informacion de modulo de politica publica Documentos Adopcion
 */
public class PoliticaPublicaDocAdopcionDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -575544609838261181L;
	
	private String tipoDocumento;
	private Integer numero;
	private Integer annoAdopcion;
	private String nombreDocumento;
	private String rutaDocumento;
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public Integer getAnnoAdopcion() {
		return annoAdopcion;
	}
	public void setAnnoAdopcion(Integer annoAdopcion) {
		this.annoAdopcion = annoAdopcion;
	}
	public String getNombreDocumento() {
		return nombreDocumento;
	}
	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}
	public String getRutaDocumento() {
		return rutaDocumento;
	}
	public void setRutaDocumento(String rutaDocumento) {
		this.rutaDocumento = rutaDocumento;
	}
	
	
}
