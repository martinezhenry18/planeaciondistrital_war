package co.com.bogota.planeaciondistrital.dto.planAccion;

import java.io.Serializable;

public class MetaDto implements Serializable {

    private Integer ANIO;
    private Double VALOR;
    private String CODIGO_INDICADOR;
    private String USUARIO_CREACION;

    public MetaDto() {
    }


    public MetaDto(Integer ANIO, Double VALOR, String CODIGO_INDICADOR, String USUARIO_CREACION, String MENSAJE, Integer VALIDACION) {
        this.ANIO = ANIO;
        this.VALOR = VALOR;
        this.CODIGO_INDICADOR = CODIGO_INDICADOR;
        this.USUARIO_CREACION = USUARIO_CREACION;
    }

    public Integer getANIO() {
        return ANIO;
    }

    public void setANIO(Integer ANIO) {
        this.ANIO = ANIO;
    }

    public Double getVALOR() {
        return VALOR;
    }

    public void setVALOR(Double VALOR) {
        this.VALOR = VALOR;
    }

    public String getCODIGO_INDICADOR() {
        return CODIGO_INDICADOR;
    }

    public void setCODIGO_INDICADOR(String CODIGO_INDICADOR) {
        this.CODIGO_INDICADOR = CODIGO_INDICADOR;
    }

    public String getUSUARIO_CREACION() {
        return USUARIO_CREACION;
    }

    public void setUSUARIO_CREACION(String USUARIO_CREACION) {
        this.USUARIO_CREACION = USUARIO_CREACION;
    }

}
