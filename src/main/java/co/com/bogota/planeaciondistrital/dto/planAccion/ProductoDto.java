package co.com.bogota.planeaciondistrital.dto.planAccion;

import java.io.Serializable;

public class ProductoDto implements Serializable {
    private String NOMBRE_PRODUCTO;
    private Double IMPORTANCIA_RELATIVA;
    private String CODIGO_SECTOR;
    private String CODIGO_ENTIDAD;
    private String CODIGO_RESULTADO;
    private String USUARIO;

    public ProductoDto() {
    }

    public ProductoDto(String NOMBRE_PRODUCTO, Double IMPORTANCIA_RELATIVA, String CODIGO_SECTOR, String CODIGO_ENTIDAD, String CODIGO_RESULTADO, String USUARIO, String MENSAJE, Double VALIDACION) {
        this.NOMBRE_PRODUCTO = NOMBRE_PRODUCTO;
        this.IMPORTANCIA_RELATIVA = IMPORTANCIA_RELATIVA;
        this.CODIGO_SECTOR = CODIGO_SECTOR;
        this.CODIGO_ENTIDAD = CODIGO_ENTIDAD;
        this.CODIGO_RESULTADO = CODIGO_RESULTADO;
        this.USUARIO = USUARIO;
    }

    public String getNOMBRE_PRODUCTO() {
        return NOMBRE_PRODUCTO;
    }

    public void setNOMBRE_PRODUCTO(String NOMBRE_PRODUCTO) {
        this.NOMBRE_PRODUCTO = NOMBRE_PRODUCTO;
    }

    public Double getIMPORTANCIA_RELATIVA() {
        return IMPORTANCIA_RELATIVA;
    }

    public void setIMPORTANCIA_RELATIVA(Double IMPORTANCIA_RELATIVA) {
        this.IMPORTANCIA_RELATIVA = IMPORTANCIA_RELATIVA;
    }

    public String getCODIGO_SECTOR() {
        return CODIGO_SECTOR;
    }

    public void setCODIGO_SECTOR(String CODIGO_SECTOR) {
        this.CODIGO_SECTOR = CODIGO_SECTOR;
    }

    public String getCODIGO_ENTIDAD() {
        return CODIGO_ENTIDAD;
    }

    public void setCODIGO_ENTIDAD(String CODIGO_ENTIDAD) {
        this.CODIGO_ENTIDAD = CODIGO_ENTIDAD;
    }

    public String getCODIGO_RESULTADO() {
        return CODIGO_RESULTADO;
    }

    public void setCODIGO_RESULTADO(String CODIGO_RESULTADO) {
        this.CODIGO_RESULTADO = CODIGO_RESULTADO;
    }

    public String getUSUARIO() {
        return USUARIO;
    }

    public void setUSUARIO(String USUARIO) {
        this.USUARIO = USUARIO;
    }


}
