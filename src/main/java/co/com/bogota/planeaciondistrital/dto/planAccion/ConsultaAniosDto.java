package co.com.bogota.planeaciondistrital.dto.planAccion;

import java.io.Serializable;

public class ConsultaAniosDto implements Serializable {

    Integer ANIO;
    Integer VALOR;

    public ConsultaAniosDto() {
    }

    public ConsultaAniosDto(Integer ANIO, Integer VALOR) {
        this.ANIO = ANIO;
        this.VALOR = VALOR;
    }

    public Integer getANIO() {
        return ANIO;
    }

    public void setANIO(Integer ANIO) {
        this.ANIO = ANIO;
    }

    public Integer getVALOR() {
        return VALOR;
    }

    public void setVALOR(Integer VALOR) {
        this.VALOR = VALOR;
    }

    @Override
    public String toString() {
        return "{" +
                "ANIO=" + ANIO +
                ", VALOR=" + VALOR +
                '}';
    }
}
