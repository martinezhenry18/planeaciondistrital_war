package co.com.bogota.planeaciondistrital.dto;

import java.io.Serializable;

/**
 * 
 * @author Daniel Arebey Sarmiento Amaya
 *
 */
public class StatusResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3721592554684710512L;
	
	private Integer statusId;
	private String statusDescription;
	
	public StatusResponse(Integer statusId,String statusDescription) {
		this.statusId = statusId;
		this.statusDescription = statusDescription;
	}
	
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
}
