package co.com.bogota.planeaciondistrital.dto;

import java.io.Serializable;

/**
 * 
 * @author Daniel Sarmiento Amaya
 * @see Contiene la informacion de modulo de politica publica Documentos Asociados
 */
public class PoliticaPulbicaAsociados implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7082109969080141620L;
	
	private String tipoDocumentoAsociado;
	private String nombreDocumentoAsociado;
	private String rutaDocumento;
	
	public String getTipoDocumentoAsociado() {
		return tipoDocumentoAsociado;
	}
	public void setTipoDocumentoAsociado(String tipoDocumentoAsociado) {
		this.tipoDocumentoAsociado = tipoDocumentoAsociado;
	}
	public String getNombreDocumentoAsociado() {
		return nombreDocumentoAsociado;
	}
	public void setNombreDocumentoAsociado(String nombreDocumentoAsociado) {
		this.nombreDocumentoAsociado = nombreDocumentoAsociado;
	}
	public String getRutaDocumento() {
		return rutaDocumento;
	}
	public void setRutaDocumento(String rutaDocumento) {
		this.rutaDocumento = rutaDocumento;
	}
	
	
}
