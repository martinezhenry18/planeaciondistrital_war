package co.com.bogota.planeaciondistrital.dto.parametrizacion;

import java.io.Serializable;

/**
	*	@author 	Henry Martinez
	*	@empresa	Ares Soluciones S.A.S
	*	@mail		hmartinez@aressoluciones.com
	*	@fecha 		20/05/2019
	*	@proyeco	Planeacion Distrital
	*	@clase 		ParametrizacionListasBasicasDto.java
	*	@comments	
*/



public class ParametrizacionListasBasicasDto implements Serializable
{
	
	

	private static final long serialVersionUID = -1L;
	private String idLista;
	private String nombreLista;
	private String descripcionLista = null;
	private String estadoLista;
	private String valorLista = null;
	private Integer tipoParametroLista = null;
	
	
	
	public String getIdLista()
	{
		return idLista;
	}
	public void setIdLista(String idLista)
	{
		this.idLista = idLista;
	}
	public String getNombreLista()
	{
		return nombreLista;
	}
	public void setNombreLista(String nombreLista)
	{
		this.nombreLista = nombreLista;
	}
	public String getEstadoLista()
	{
		return estadoLista;
	}
	public void setEstadoLista(String estadoLista)
	{
		this.estadoLista = estadoLista;
	}
	public String getDescripcionLista()
	{
		return descripcionLista;
	}
	public void setDescripcionLista(String descripcionLista)
	{
		this.descripcionLista = descripcionLista;
	}
	public String getValorLista()
	{
		return valorLista;
	}
	public void setValorLista(String valorLista)
	{
		this.valorLista = valorLista;
	}
	public Integer getTipoParametroLista()
	{
		return tipoParametroLista;
	}
	public void setTipoParametroLista(Integer tipoParametroLista)
	{
		this.tipoParametroLista = tipoParametroLista;
	}
	
	
	
	
	
	
	
}
