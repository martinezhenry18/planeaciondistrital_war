package co.com.bogota.planeaciondistrital.dto;

import java.io.Serializable;

/**
 * 
 * @author Daniel Sarmiento Amaya
 * @see Contiene la informacion de modulo de politica publica objetivos
 */
public class PoliticaPulbicaObjetivosDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5188199980276740657L;
	
	private String objetivoGeneral;
	private String objetivoEspecifico;
	
	public String getObjetivoGeneral() {
		return objetivoGeneral;
	}
	public void setObjetivoGeneral(String objetivoGeneral) {
		this.objetivoGeneral = objetivoGeneral;
	}
	public String getObjetivoEspecifico() {
		return objetivoEspecifico;
	}
	public void setObjetivoEspecifico(String objetivoEspecifico) {
		this.objetivoEspecifico = objetivoEspecifico;
	}
	
	
}
