package co.com.bogota.planeaciondistrital.dto.planAccion;

import java.io.Serializable;

public class ObjetivoEspecificoDto implements Serializable {
    private String CODIGO_OBJETIVO_ESPECIFICO;
    private String OBJETIVO_ESPECIFICO;

    public ObjetivoEspecificoDto(String CODIGO_OBJETIVO_ESPECIFICO, String OBJETIVO_ESPECIFICO) {
        this.CODIGO_OBJETIVO_ESPECIFICO = CODIGO_OBJETIVO_ESPECIFICO;
        this.OBJETIVO_ESPECIFICO = OBJETIVO_ESPECIFICO;
    }

    public ObjetivoEspecificoDto() {
    }

    public String getCODIGO_OBJETIVO_ESPECIFICO() {
        return CODIGO_OBJETIVO_ESPECIFICO;
    }

    public void setCODIGO_OBJETIVO_ESPECIFICO(String CODIGO_OBJETIVO_ESPECIFICO) {
        this.CODIGO_OBJETIVO_ESPECIFICO = CODIGO_OBJETIVO_ESPECIFICO;
    }

    public String getOBJETIVO_ESPECIFICO() {
        return OBJETIVO_ESPECIFICO;
    }

    public void setOBJETIVO_ESPECIFICO(String OBJETIVO_ESPECIFICO) {
        this.OBJETIVO_ESPECIFICO = OBJETIVO_ESPECIFICO;
    }

}
