package co.com.bogota.planeaciondistrital.dto.planAccion;

import java.io.Serializable;

public class ResultadoDto implements Serializable {

    private String CODIGO_POLITICA;
    private Integer CODIGO_OBJETIVO;
    private String NOMBRE_RESULTADO;
    private String CODIGO_SECTOR;
    private String CODIGO_ENTIDAD;
    private Double IMPORTANCIA_RELATIVA;
    private String USUARIO;
    private String CODIGO_RESULTADO;

    public ResultadoDto() {
    }

    public ResultadoDto(String CODIGO_POLITICA, Integer CODIGO_OBJETIVO, String NOMBRE_RESULTADO, String CODIGO_SECTOR, String CODIGO_ENTIDAD, Double IMPORTANCIA_RELATIVA, String USUARIO, String CODIGO_RESULTADO) {
        this.CODIGO_POLITICA = CODIGO_POLITICA;
        this.CODIGO_OBJETIVO = CODIGO_OBJETIVO;
        this.NOMBRE_RESULTADO = NOMBRE_RESULTADO;
        this.CODIGO_SECTOR = CODIGO_SECTOR;
        this.CODIGO_ENTIDAD = CODIGO_ENTIDAD;
        this.IMPORTANCIA_RELATIVA = IMPORTANCIA_RELATIVA;
        this.USUARIO = USUARIO;
        this.CODIGO_RESULTADO = CODIGO_RESULTADO;
    }

    public String getCODIGO_POLITICA() {
        return CODIGO_POLITICA;
    }

    public void setCODIGO_POLITICA(String CODIGO_POLITICA) {
        this.CODIGO_POLITICA = CODIGO_POLITICA;
    }


    public String getNOMBRE_RESULTADO() {
        return NOMBRE_RESULTADO;
    }

    public void setNOMBRE_RESULTADO(String NOMBRE_RESULTADO) {
        this.NOMBRE_RESULTADO = NOMBRE_RESULTADO;
    }

    public String getCODIGO_SECTOR() {
        return CODIGO_SECTOR;
    }

    public void setCODIGO_SECTOR(String CODIGO_SECTOR) {
        this.CODIGO_SECTOR = CODIGO_SECTOR;
    }

    public String getCODIGO_ENTIDAD() {
        return CODIGO_ENTIDAD;
    }

    public void setCODIGO_ENTIDAD(String CODIGO_ENTIDAD) {
        this.CODIGO_ENTIDAD = CODIGO_ENTIDAD;
    }

    public String getUSUARIO() {
        return USUARIO;
    }

    public void setUSUARIO(String USUARIO) {
        this.USUARIO = USUARIO;
    }

    public String getCODIGO_RESULTADO() {
        return CODIGO_RESULTADO;
    }

    public void setCODIGO_RESULTADO(String CODIGO_RESULTADO) {
        this.CODIGO_RESULTADO = CODIGO_RESULTADO;
    }

    public Integer getCODIGO_OBJETIVO() {
        return CODIGO_OBJETIVO;
    }

    public void setCODIGO_OBJETIVO(Integer CODIGO_OBJETIVO) {
        this.CODIGO_OBJETIVO = CODIGO_OBJETIVO;
    }

    public Double getIMPORTANCIA_RELATIVA() {
        return IMPORTANCIA_RELATIVA;
    }

    public void setIMPORTANCIA_RELATIVA(Double IMPORTANCIA_RELATIVA) {
        this.IMPORTANCIA_RELATIVA = IMPORTANCIA_RELATIVA;
    }

}
