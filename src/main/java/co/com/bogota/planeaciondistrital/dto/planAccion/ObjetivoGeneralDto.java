package co.com.bogota.planeaciondistrital.dto.planAccion;

import java.io.Serializable;
import java.util.List;

public class ObjetivoGeneralDto implements Serializable {
    private String CODIGO_OBJETIVO;
    private String OBJETIVO_GENERAL;
    private List<ObjetivoEspecificoDto> OBJETIVOS_ESPECIFICOS;


    public ObjetivoGeneralDto() {
    }

    public ObjetivoGeneralDto(String CODIGO_OBJETIVO, String OBJETIVO_GENERAL, List<ObjetivoEspecificoDto> OBJETIVOS_ESPECIFICOS) {
        this.CODIGO_OBJETIVO = CODIGO_OBJETIVO;
        this.OBJETIVO_GENERAL = OBJETIVO_GENERAL;
        this.OBJETIVOS_ESPECIFICOS = OBJETIVOS_ESPECIFICOS;
    }

    public String getCODIGO_OBJETIVO() {
        return CODIGO_OBJETIVO;
    }

    public void setCODIGO_OBJETIVO(String CODIGO_OBJETIVO) {
        this.CODIGO_OBJETIVO = CODIGO_OBJETIVO;
    }

    public String getOBJETIVO_GENERAL() {
        return OBJETIVO_GENERAL;
    }

    public void setOBJETIVO_GENERAL(String OBJETIVO_GENERAL) {
        this.OBJETIVO_GENERAL = OBJETIVO_GENERAL;
    }

    public List<ObjetivoEspecificoDto> getOBJETIVOS_ESPECIFICOS() {
        return OBJETIVOS_ESPECIFICOS;
    }

    public void setOBJETIVOS_ESPECIFICOS(List<ObjetivoEspecificoDto> OBJETIVOS_ESPECIFICOS) {
        this.OBJETIVOS_ESPECIFICOS = OBJETIVOS_ESPECIFICOS;
    }

}
