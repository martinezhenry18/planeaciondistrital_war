package co.com.bogota.planeaciondistrital.dto.parametrizacion;

import java.io.Serializable;

/**
	*	@author 	Henry Martinez
	*	@empresa	Ares Soluciones S.A.S
	*	@mail		hmartinez@aressoluciones.com
	*	@fecha 		20/05/2019
	*	@proyeco	Planeacion Distrital
	*	@clase 		ParametrizacionListaDetalleDto.java
	*	@comments	
 */

public class ParametrizacionListaDetalleDto implements Serializable
{
	


	private static final long serialVersionUID = 8248592462316809361L;
	private Integer idParametro;
	private String nombreParametro;
	private String descripcionParametro;
	private String estadoParametro;
	private String nombreParametroPadre = null;
	
	private Integer idParametroPadre = null;
	private Integer idParametroListaBasica = null;
	private Double desdeRangoSemaforo = null;
	private Double hastaRangoSemaforo = null;
	private String colorRangoSemaforo = null;
	
		
	

	
	public Integer getIdParametro()
	{
		return idParametro;
	}
	public void setIdParametro(Integer idParametro)
	{
		this.idParametro = idParametro;
	}
	public String getNombreParametro()
	{
		return nombreParametro;
	}
	public void setNombreParametro(String nombreParametro)
	{
		this.nombreParametro = nombreParametro;
	}
	public String getEstadoParametro()
	{
		return estadoParametro;
	}
	public void setEstadoParametro(String estadoParametro)
	{
		this.estadoParametro = estadoParametro;
	}


	public String getDescripcionParametro()
	{
		return descripcionParametro;
	}


	public void setDescripcionParametro(String descripcionParametro)
	{
		this.descripcionParametro = descripcionParametro;
	}
	public Integer getIdParametroPadre()
	{
		return idParametroPadre;
	}
	public void setIdParametroPadre(Integer idParametroPadre)
	{
		this.idParametroPadre = idParametroPadre;
	}
	public Integer getIdParametroListaBasica()
	{
		return idParametroListaBasica;
	}
	public void setIdParametroListaBasica(Integer idParametroListaBasica)
	{
		this.idParametroListaBasica = idParametroListaBasica;
	}
	public String getNombreParametroPadre()
	{
		return nombreParametroPadre;
	}
	public void setNombreParametroPadre(String nombreParametroPadre)
	{
		this.nombreParametroPadre = nombreParametroPadre;
	}

	public Double getDesdeRangoSemaforo()
	{
		return desdeRangoSemaforo;
	}
	public void setDesdeRangoSemaforo(Double desdeRangoSemaforo)
	{
		this.desdeRangoSemaforo = desdeRangoSemaforo;
	}
	public Double getHastaRangoSemaforo()
	{
		return hastaRangoSemaforo;
	}
	public void setHastaRangoSemaforo(Double hastaRangoSemaforo)
	{
		this.hastaRangoSemaforo = hastaRangoSemaforo;
	}
	public String getColorRangoSemaforo()
	{
		return colorRangoSemaforo;
	}
	public void setColorRangoSemaforo(String colorRangoSemaforo)
	{
		this.colorRangoSemaforo = colorRangoSemaforo;
	}

	
	


	
	
	
}
