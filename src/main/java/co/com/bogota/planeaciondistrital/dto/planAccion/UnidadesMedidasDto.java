package co.com.bogota.planeaciondistrital.dto.planAccion;

import java.io.Serializable;

public class UnidadesMedidasDto implements Serializable {
    private String NOMBRE;
    private String VALOR;

    public UnidadesMedidasDto() {
    }

    public UnidadesMedidasDto(String NOMBRE, String VALOR) {
        this.NOMBRE = NOMBRE;
        this.VALOR = VALOR;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public String getVALOR() {
        return VALOR;
    }

    public void setVALOR(String VALOR) {
        this.VALOR = VALOR;
    }



}
