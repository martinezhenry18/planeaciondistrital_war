package co.com.bogota.planeaciondistrital.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

/**
 * 
 * @author Daniel Sarmiento Amaya
 * @see Contiene la informacion de modulo de politica publica Datos Generales
 */
public class PoliticaPublicaDatosGeneralesDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4572678768706456701L;
	
	@NotNull
	private String nombrePolitica;
	private Integer nemotDocumento;
	private Integer anno;
	private String fechaInicio;
	private String fechaFin;
	private String sectorLider;
	private String entidadLider;
	private String costoTotalPolitica;
	private String fechaAprobacion;
	private String sectoresCorresponsales[];
	
	public String getNombrePolitica() {
		return nombrePolitica;
	}
	public void setNombrePolitica(String nombrePolitica) {
		this.nombrePolitica = nombrePolitica;
	}
	public Integer getNemotDocumento() {
		return nemotDocumento;
	}
	public void setNemotDocumento(Integer nemotDocumento) {
		this.nemotDocumento = nemotDocumento;
	}
	public Integer getAnno() {
		return anno;
	}
	public void setAnno(Integer anno) {
		this.anno = anno;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getSectorLider() {
		return sectorLider;
	}
	public void setSectorLider(String sectorLider) {
		this.sectorLider = sectorLider;
	}
	public String getEntidadLider() {
		return entidadLider;
	}
	public void setEntidadLider(String entidadLider) {
		this.entidadLider = entidadLider;
	}
	public String getCostoTotalPolitica() {
		return costoTotalPolitica;
	}
	public void setCostoTotalPolitica(String costoTotalPolitica) {
		this.costoTotalPolitica = costoTotalPolitica;
	}
	public String getFechaAprobacion() {
		return fechaAprobacion;
	}
	public void setFechaAprobacion(String fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	public String[] getSectoresCorresponsales() {
		return sectoresCorresponsales;
	}
	public void setSectoresCorresponsales(String[] sectoresCorresponsales) {
		this.sectoresCorresponsales = sectoresCorresponsales;
	}
	

}
