package co.com.bogota.planeaciondistrital.dto;

import java.io.Serializable;

/**
	*	@author 	Henry Martinez
	*	@empresa	Ares Soluciones S.A.S
	*	@mail		hmartinez@aressoluciones.com
	*	@fecha 		21/05/2019
	*	@proyeco	Planeacion Distrital
	*	@clase 		ResponseGeneralDto.java
	*	@comments	
 */

public class ResponseGeneralDto implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8248335266229049829L;
	private String mensajeResponse;
	private String idResponse;
	
	
	public String getMensajeResponse()
	{
		return mensajeResponse;
	}
	public void setMensajeResponse(String mensajeResponse)
	{
		this.mensajeResponse = mensajeResponse;
	}
	public String getIdResponse()
	{
		return idResponse;
	}
	public void setIdResponse(String idResponse)
	{
		this.idResponse = idResponse;
	}
	
	
	
}
