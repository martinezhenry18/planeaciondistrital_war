package co.com.bogota.planeaciondistrital.controller;

import java.text.ParseException;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.bogota.planeaciondistrital.Constants;
import co.com.bogota.planeaciondistrital.component.ConvertDtoModel;
import co.com.bogota.planeaciondistrital.dto.PoliticaPublicaDatosGeneralesDto;
import co.com.bogota.planeaciondistrital.dto.StatusResponse;
import co.com.bogota.planeaciondistrital.exception.PlaneacionException;
import co.com.bogota.planeaciondistrital.model.PoliticaPublicaDatosGeneralesModel;
import co.com.bogota.planeaciondistrital.service.PoliticaPublicaService;
import io.swagger.annotations.Api;

/**
 * 
 * @author Daniel Arbey Sarmiento Amaya
 * @see Api rest modulo politica publica
 *
 */
@RestController
@RequestMapping("/api/politicaPublica")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT })
public class PoliticaPublicaRestController {
	private static Logger logger = LoggerFactory.getLogger(PoliticaPublicaRestController.class);

	@Autowired
	private ConvertDtoModel convertDtoModel;

	@Autowired
	private PoliticaPublicaService politicaPublicaService;

	@PostMapping("/datosGenerales")
	ResponseEntity<StatusResponse> guardarDatosGenerales(@Valid @RequestBody PoliticaPublicaDatosGeneralesDto request) {
		logger.info("--Guardar datos Generales--");
		try {
			PoliticaPublicaDatosGeneralesModel model = convertDtoModel.convertPoliticaPublicaDatosGenerales(request);

			politicaPublicaService.guardarDatosGenerales(model);
			return new ResponseEntity<>(new StatusResponse(Constants.OK_INFO, ""), HttpStatus.OK);

		} catch (PlaneacionException ae) {
			logger.error(ae.getMessage());
			if (ae.getMessage().contains("0&")) {
				return new ResponseEntity<>(
						new StatusResponse(Constants.EER_INFO, ae.getMessage().split(Constants.CHARTER_SPLIT)[1]),
						HttpStatus.CONFLICT);
			}
			return new ResponseEntity<>(new StatusResponse(Constants.EER_INFO, "Error del sistema"),
					HttpStatus.CONFLICT);
		} catch (ParseException pe) {
			logger.error(pe.getMessage());
			return new ResponseEntity<StatusResponse>(new StatusResponse(Constants.EER_DATA, "Error del sistema"),
					HttpStatus.CONFLICT);
		}
	}
}
