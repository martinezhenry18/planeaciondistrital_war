package co.com.bogota.planeaciondistrital.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.validation.Valid;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.bogota.planeaciondistrital.component.ConvertDtoModel;
import co.com.bogota.planeaciondistrital.component.ConvertModelDto;
import co.com.bogota.planeaciondistrital.dto.ResponseGeneralDto;
import co.com.bogota.planeaciondistrital.dto.parametrizacion.ParametrizacionListaDetalleDto;
import co.com.bogota.planeaciondistrital.dto.parametrizacion.ParametrizacionListasBasicasDto;
import co.com.bogota.planeaciondistrital.exception.PlaneacionException;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListaDetalleModel;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListasBasicasModel;
import co.com.bogota.planeaciondistrital.service.ParametrizacionService;

/**
	*	@author 	Henry Martinez
	*	@empresa	Ares Soluciones S.A.S
	*	@mail		hmartinez@aressoluciones.com
	*	@fecha 		20/05/2019
	*	@proyecto	Planeacion Distrital
	*	@clase 		ParametrosRestController.java
	*	@comments	
 */

@RestController
@RequestMapping("/api/parametrizacion")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
public class ParametrizacionRestController
{
	
	private static Logger logger = LoggerFactory.getLogger(ParametrizacionRestController.class);

	@Autowired
	private ParametrizacionService parametrizacionService;
	
	@Autowired
	private ConvertDtoModel convertDtoModel;
	
	@Autowired
	private ConvertModelDto convertModelDto;
	
	private ResponseGeneralDto objResponseGeneralDto;
	
	private JSONObject obj;

	

	
	
	@GetMapping(value = "/listasBasicas", produces = "application/json; charset=UTF-8")
	public ResponseEntity<String> getListasBasicas() throws ParseException, SQLException, JSONException 
	{
		logger.info("--Consulta Lista Basica--");
		ArrayList<ParametrizacionListasBasicasModel> result = parametrizacionService.getListasBasicas();
		ArrayList<ParametrizacionListasBasicasDto> listParametrizacionListasBasicasDto = new ArrayList<ParametrizacionListasBasicasDto>();

		
		for (ParametrizacionListasBasicasModel  m: result)
		{
			listParametrizacionListasBasicasDto.add(convertModelDto.convertListaBasicaDto(m));
		}
		
		
		//parametrizacionService.getListaParametros();
		obj = new JSONObject();
		obj.put("listasBasicas",listParametrizacionListasBasicasDto);
		
		return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/listasBasicasDetalle", produces = "application/json; charset=UTF-8")
	public ResponseEntity<String> getListaDetalle(@RequestParam Integer idListaBasica) throws ParseException, SQLException, JSONException 
	{
		logger.info("--Consulta detalle de lista basica--");
		int idLista = idListaBasica;
		
		
		ArrayList<ParametrizacionListaDetalleModel> result = parametrizacionService.getListaParametroDetalle(idLista);
		ArrayList<ParametrizacionListaDetalleDto> listParametrizacionListaDetalleDto = new ArrayList<ParametrizacionListaDetalleDto>();
		
		for (ParametrizacionListaDetalleModel  m: result)
		{
			listParametrizacionListaDetalleDto.add(convertModelDto.convertListaDetalleDto(m));
		}
		
		
		obj = new JSONObject();
		obj.put("listaDetalle",listParametrizacionListaDetalleDto);
		
		return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
	}
	
	@PostMapping(value = "/actualizaParametroListaBasica", produces = "application/json; charset=UTF-8")
	ResponseEntity<String> actualizaParametroListaBasica(@Valid @RequestBody ParametrizacionListasBasicasDto request) throws SQLException, JSONException {
    	logger.info("--Actualizar Parametro Detalle--");
    	try {
    		ParametrizacionListasBasicasModel modelParametrizacionListasBasicasDto = convertDtoModel.convertParametrizacionListaBasicaDto(request);
    		
    		setObjResponseGeneralDto(parametrizacionService.actualizarParametroListaBasica(modelParametrizacionListasBasicasDto));
    		
    		ArrayList<ResponseGeneralDto> listResponseGeneralDto = new ArrayList<>();
    		
    		listResponseGeneralDto.add(objResponseGeneralDto);
    		
    		obj = new JSONObject();
    		obj.put("response",listResponseGeneralDto);
    		
    		
    		return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
 
		}catch(PlaneacionException ae) {
			logger.error(ae.getMessage());
			if(ae.getMessage().contains("0&")) {
				return new ResponseEntity<String>(obj.toString(), HttpStatus.CONFLICT);			
			}
			return new ResponseEntity<String>(obj.toString(), HttpStatus.CONFLICT);
		}catch(ParseException pe) {
			logger.error(pe.getMessage());
			return new ResponseEntity<String>(obj.toString(), HttpStatus.CONFLICT);
		}
	}
		
	@PostMapping(value = "/insertaDetalleListaBasica", produces = "application/json; charset=UTF-8")
	ResponseEntity<String> insertaDetalleListaBasica(@Valid @RequestBody ParametrizacionListaDetalleDto request) throws SQLException, JSONException 
	{
    	logger.info("--Guardar Parametro Detalle--");
    	try {
    		ParametrizacionListaDetalleModel modelParametrizacionListaDetalleDto = convertDtoModel.convertParametrizacionListaDetalle(request);
    		
    		setObjResponseGeneralDto(parametrizacionService.guardarDetalleListaBasica(modelParametrizacionListaDetalleDto));
    		
    		ArrayList<ResponseGeneralDto> listResponseGeneralDto = new ArrayList<>();
    		
    		listResponseGeneralDto.add(objResponseGeneralDto);
    		
    		obj = new JSONObject();
    		obj.put("response",listResponseGeneralDto);
    		
    		
    		return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
 
		}catch(PlaneacionException ae) {
			logger.error(ae.getMessage());
			if(ae.getMessage().contains("0&")) {
				return new ResponseEntity<String>(obj.toString(), HttpStatus.CONFLICT);			
			}
			return new ResponseEntity<String>(obj.toString(), HttpStatus.CONFLICT);
		}catch(ParseException pe) {
			logger.error(pe.getMessage());
			return new ResponseEntity<String>(obj.toString(), HttpStatus.CONFLICT);
		}
	}
	
	@PostMapping(value = "/actualizaDetalleListaBasica", produces = "application/json; charset=UTF-8")
	ResponseEntity<String> actualizaDetalleListaBasica(@Valid @RequestBody ParametrizacionListaDetalleDto request) throws SQLException, JSONException {
    	logger.info("--Actualizar Parametro Detalle--");
    	try {
    		ParametrizacionListaDetalleModel modelParametrizacionListaDetalleDto = convertDtoModel.convertParametrizacionListaDetalle(request);
    		
    		setObjResponseGeneralDto(parametrizacionService.actualizarDetalleListaBasica(modelParametrizacionListaDetalleDto));
    		
    		ArrayList<ResponseGeneralDto> listResponseGeneralDto = new ArrayList<>();
    		
    		listResponseGeneralDto.add(objResponseGeneralDto);
    		
    		obj = new JSONObject();
    		obj.put("response",listResponseGeneralDto);
    		
    		
    		return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
 
		}catch(PlaneacionException ae) {
			logger.error(ae.getMessage());
			if(ae.getMessage().contains("0&")) {
				return new ResponseEntity<String>(obj.toString(), HttpStatus.CONFLICT);			
			}
			return new ResponseEntity<String>(obj.toString(), HttpStatus.CONFLICT);
		}catch(ParseException pe) {
			logger.error(pe.getMessage());
			return new ResponseEntity<String>(obj.toString(), HttpStatus.CONFLICT);
		}
	}


	public ResponseGeneralDto getObjResponseGeneralDto()
	{
		return objResponseGeneralDto;
	}


	public void setObjResponseGeneralDto(ResponseGeneralDto objResponseGeneralDto)
	{
		this.objResponseGeneralDto = objResponseGeneralDto;
	}
	
	
}
