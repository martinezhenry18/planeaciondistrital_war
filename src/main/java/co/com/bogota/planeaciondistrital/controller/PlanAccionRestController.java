package co.com.bogota.planeaciondistrital.controller;

import co.com.bogota.planeaciondistrital.Constants;
import co.com.bogota.planeaciondistrital.component.ConvertDtoModel;
import co.com.bogota.planeaciondistrital.component.ConvertModelDto;
import co.com.bogota.planeaciondistrital.dto.StatusResponse;
import co.com.bogota.planeaciondistrital.dto.planAccion.IndicadorDto;
import co.com.bogota.planeaciondistrital.dto.planAccion.MetaDto;
import co.com.bogota.planeaciondistrital.dto.planAccion.ProductoDto;
import co.com.bogota.planeaciondistrital.dto.planAccion.ResultadoDto;
import co.com.bogota.planeaciondistrital.exception.PlaneacionException;
import co.com.bogota.planeaciondistrital.model.planAccion.*;
import co.com.bogota.planeaciondistrital.service.PlanAccionService;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/planAccion")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class PlanAccionRestController {

    private static Logger logger = LoggerFactory.getLogger(PlanAccionRestController.class);

    @Autowired
    private ObjectMapper mapper;


    @Autowired
    private ConvertDtoModel convertDtoModel;
    @Autowired
    private ConvertModelDto convertModelDto;
    @Autowired
    private PlanAccionService planAccionService;

    @GetMapping(value = "/consultarObjetivos", produces = "application/json; charset=UTF-8")
    ResponseEntity<String> consultarObjetivos(@Valid @RequestHeader String P_CODIGO_POLITICA) {
        logger.info("--Consultar Objetivos--");
        try {
            List<respuestaConsultaObjetivosModel> response = planAccionService.consultarObjetivo(P_CODIGO_POLITICA);
            JSONObject obj = new JSONObject();
            obj.put("objetivosGenerales", convertModelDto.convertRespuestaConsultaObjetivoModel(response));

            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);

        } catch (PlaneacionException ae) {
            logger.error(ae.getMessage());
            if (ae.getMessage().contains("0&")) {
                return new ResponseEntity<String>(ae.getMessage().split(Constants.CHARTER_SPLIT)[1], HttpStatus.CONFLICT);
            }
            return new ResponseEntity<String>(ae.getMessage(), HttpStatus.CONFLICT);
        } catch (ParseException pe) {
            logger.error(pe.getMessage());
            return new ResponseEntity<String>(pe.getMessage(), HttpStatus.CONFLICT);
        } catch (SQLException se) {
            logger.error(se.getMessage());
            if (se.getMessage().contains("0&")) {
                return new ResponseEntity<String>(se.getMessage(), HttpStatus.BAD_REQUEST);
            }
        } catch (JSONException je) {
            logger.error(je.getMessage());
            return new ResponseEntity<String>(je.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "/consultarSectores", produces = "application/json; charset=UTF-8")
    ResponseEntity<String> consultarSectores() {
        logger.info("--Consultar Sectores--");
        try {
            List<SectoresModel> response = planAccionService.ConsultarSectores();
            JSONObject obj = new JSONObject();
            obj.put("Sectores", convertModelDto.convertSectoresModel(response));

            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);

        } catch (ParseException pe) {
            logger.error(pe.getMessage());
            return new ResponseEntity<String>(pe.getMessage(), HttpStatus.CONFLICT);
        } catch (SQLException se) {
            logger.error(se.getMessage());
            if (se.getMessage().contains("0&")) {
                return new ResponseEntity<String>(se.getMessage(), HttpStatus.BAD_REQUEST);
            }
        } catch (JSONException je) {
            logger.error(je.getMessage());
            return new ResponseEntity<String>(je.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "/consultarEntidades", produces = "application/json; charset=UTF-8")
    ResponseEntity<String> consultarEntidades(@Valid @RequestHeader String P_VALOR_SECTOR) {
        logger.info("--Consultar Entidades--");
        try {
            List<EntidadesModel> response = planAccionService.ConsultarEntidades(P_VALOR_SECTOR);
            JSONObject obj = new JSONObject();
            obj.put("Entidades", convertModelDto.convertEntidadesModel(response));

            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);

        } catch (ParseException pe) {
            logger.error(pe.getMessage());
            return new ResponseEntity<String>(pe.getMessage(), HttpStatus.CONFLICT);
        } catch (SQLException se) {
            logger.error(se.getMessage());
            if (se.getMessage().contains("0&")) {
                return new ResponseEntity<String>(se.getMessage(), HttpStatus.BAD_REQUEST);
            }
        } catch (JSONException je) {
            logger.error(je.getMessage());
            return new ResponseEntity<String>(je.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "/consultarTiposAnualizaciones", produces = "application/json; charset=UTF-8")
    ResponseEntity<String> consultarTiposAnualizaciones() {
        logger.info("--Consultar Tipos Anualizaciones--");
        try {
            List<TipoAnualizacionModel> response = planAccionService.ConsultarTiposAnualizacion();
            JSONObject obj = new JSONObject();
            obj.put("TiposAnualizaciones", convertModelDto.convertTiposAnualizacionesModel(response));

            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);

        } catch (ParseException pe) {
            logger.error(pe.getMessage());
            return new ResponseEntity<String>(pe.getMessage(), HttpStatus.CONFLICT);
        } catch (SQLException se) {
            logger.error(se.getMessage());
            if (se.getMessage().contains("0&")) {
                return new ResponseEntity<String>(se.getMessage(), HttpStatus.BAD_REQUEST);
            }
        } catch (JSONException je) {
            logger.error(je.getMessage());
            return new ResponseEntity<String>(je.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "/consultarUnidadesMedidas", produces = "application/json; charset=UTF-8")
    ResponseEntity<String> consultarUnidadesMedidas() {
        logger.info("--Consultar Unidades Medidas--");
        try {
            List<UnidadesMedidasModel> response = planAccionService.ConsultarUnidadesMedidas();
            JSONObject obj = new JSONObject();
            obj.put("UnidadesMedidas", convertModelDto.convertUnidadesMedidasModel(response));

            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);

        } catch (ParseException pe) {
            logger.error(pe.getMessage());
            return new ResponseEntity<String>(pe.getMessage(), HttpStatus.CONFLICT);
        } catch (SQLException se) {
            logger.error(se.getMessage());
            if (se.getMessage().contains("0&")) {
                return new ResponseEntity<String>(se.getMessage(), HttpStatus.BAD_REQUEST);
            }
        } catch (JSONException je) {
            logger.error(je.getMessage());
            return new ResponseEntity<String>(je.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "/consultarAnios", produces = "application/json; charset=UTF-8")
    ResponseEntity<String> consultarAnios(@RequestHeader String P_CODIGO_INDICADOR) {
        logger.info("--Consultar Anios--");
        try {
            List<ConsultaAniosModel> response = planAccionService.ConsultarAnios(P_CODIGO_INDICADOR);
            JSONObject obj = new JSONObject();
            obj.put("ConsultaAnios", convertModelDto.convertAniosModel(response));

            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);

        } catch (ParseException pe) {
            logger.error(pe.getMessage());
            return new ResponseEntity<String>(pe.getMessage(), HttpStatus.CONFLICT);
        } catch (SQLException se) {
            logger.error(se.getMessage());
            if (se.getMessage().contains("0&")) {
                return new ResponseEntity<String>(se.getMessage(), HttpStatus.BAD_REQUEST);
            }
        } catch (JSONException je) {
            logger.error(je.getMessage());
            return new ResponseEntity<String>(je.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
    }

    @PostMapping(value = "/resultados", produces = "application/json; charset=UTF-8")
    ResponseEntity<String> insertarResultados(@Valid @RequestBody ResultadoDto request) {
        logger.info("--Insertar Resultados--");
        try {

            ResultadoModel modelo = convertDtoModel.convertResultadoDto(request);
            ResultadoModel response = planAccionService.InsertarResultado(modelo);

            if (response.getVALIDACION() == 0) {
                logger.error("--Error Procedimiento-- : " + response.getMENSAJE());
                return new ResponseEntity<String>(response.getMENSAJE(), HttpStatus.CONFLICT);
            }
            List<ResultadoDto> list = new ArrayList<>();
            list.add(convertModelDto.convertResultadoModel(response));
            JSONObject obj = new JSONObject();
            obj.put("Resultados",list);

            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);

        } catch (ParseException pe) {
            logger.error(pe.getMessage());
            return new ResponseEntity<String>(pe.getMessage(), HttpStatus.CONFLICT);
        } catch (SQLException se) {
            logger.error(se.getMessage());
            if (se.getMessage().contains("0&")) {
                return new ResponseEntity<String>(se.getMessage(), HttpStatus.BAD_REQUEST);
            }
        } catch (JSONException je) {
            logger.error(je.getMessage());
            return new ResponseEntity<String>(je.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
    }

    @PostMapping(value = "/indicador", produces = "application/json; charset=UTF-8")
    ResponseEntity<String> insertarIndicador(@Valid @RequestBody IndicadorDto request) {
        logger.info("--Insertar Indicador--");
        try {

            IndicadorModel modelo = convertDtoModel.convertIndicadorDto(request);
            IndicadorModel response = planAccionService.InsertarIndicador(modelo);

            if (response.getVALIDACION() == 0) {
                logger.error("--Error Procedimiento-- : " + response.getMENSAJE());
                return new ResponseEntity<String>(response.getMENSAJE(), HttpStatus.CONFLICT);
            }
            List<IndicadorDto> list = new ArrayList<>();
            list.add(convertModelDto.convertIndicadoresModel(response));
            JSONObject obj = new JSONObject();
            obj.put("Indicador", list);

            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);

        } catch (ParseException pe) {
            logger.error(pe.getMessage());
            return new ResponseEntity<String>(pe.getMessage(), HttpStatus.CONFLICT);
        }  catch (SQLException se) {
            logger.error(se.getMessage());
            if (se.getMessage().contains("0&")) {
                return new ResponseEntity<String>(se.getMessage(), HttpStatus.BAD_REQUEST);
            }
        } catch (JSONException je) {
            logger.error(je.getMessage());
            return new ResponseEntity<String>(je.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
    }

    @PostMapping(value = "/meta", produces = "application/json; charset=UTF-8")
    ResponseEntity<String> insertarMeta(@Valid @RequestBody MetaDto request) {
        logger.info("--Insertar Meta--");
        try {

            MetaModel modelo = convertDtoModel.convertMetaDto(request);
            MetaModel response = planAccionService.InsertarMeta(modelo);

            if (response.getVALIDACION() == 0) {
                logger.error("--Error Procedimiento-- : " + response.getMENSAJE());
                return new ResponseEntity<String>(response.getMENSAJE(), HttpStatus.CONFLICT);
            }
            List<MetaDto> list = new ArrayList<>();
            list.add(convertModelDto.convertMetaModel(response));
            JSONObject obj = new JSONObject();
            obj.put("Meta", list);

            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);

        } catch (ParseException pe) {
            logger.error(pe.getMessage());
            return new ResponseEntity<String>(pe.getMessage(), HttpStatus.CONFLICT);
        } catch (SQLException se) {
            logger.error(se.getMessage());
            if (se.getMessage().contains("0&")) {
                return new ResponseEntity<String>(se.getMessage(), HttpStatus.BAD_REQUEST);
            }
        } catch (JSONException je) {
            logger.error(je.getMessage());
            return new ResponseEntity<String>(je.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
    }

    @PostMapping(value = "/producto", produces = "application/json; charset=UTF-8")
    ResponseEntity<String> insertarProducto(@Valid @RequestBody ProductoDto request) {
        logger.info("--Insertar Producto--");
        try {

            ProductoModel modelo = convertDtoModel.convertProductoDto(request);
            ProductoModel response = planAccionService.InsertarProducto(modelo);

            if (response.getVALIDACION() == 0) {
                logger.error("--Error Procedimiento-- : " + response.getMENSAJE());
                return new ResponseEntity<String>(response.getMENSAJE(), HttpStatus.CONFLICT);
            }
            List<ProductoDto> list = new ArrayList<>();
            list.add(convertModelDto.convertProductoModel(response));
            JSONObject obj = new JSONObject();
            obj.put("Producto", list);

            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);

        } catch (ParseException pe) {
            logger.error(pe.getMessage());
            return new ResponseEntity<String>(pe.getMessage(), HttpStatus.CONFLICT);
        } catch (SQLException se) {
            logger.error(se.getMessage());
            if (se.getMessage().contains("0&")) {
                return new ResponseEntity<String>(se.getMessage(), HttpStatus.BAD_REQUEST);
            }
        } catch (JSONException je) {
            logger.error(je.getMessage());
            return new ResponseEntity<String>(je.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
    }
}



