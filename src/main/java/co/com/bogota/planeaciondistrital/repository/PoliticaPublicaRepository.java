package co.com.bogota.planeaciondistrital.repository;

import co.com.bogota.planeaciondistrital.exception.PlaneacionException;
import co.com.bogota.planeaciondistrital.model.PoliticaPublicaDatosGeneralesModel;

/**
 * 
 * @author Daniel Arbey Sarmiento Amaya
 *
 */
public interface PoliticaPublicaRepository {

	/**
	 * @see Guardar datos generales
	 * @param politicaPublicaDatosGeneralesModel
	 * @throws PlaneacionException
	 */
	void guardarDatosGenerales(PoliticaPublicaDatosGeneralesModel politicaPublicaDatosGeneralesModel) throws PlaneacionException ;
}
