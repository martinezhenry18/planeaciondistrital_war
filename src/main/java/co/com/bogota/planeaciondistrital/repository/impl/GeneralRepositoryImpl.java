package co.com.bogota.planeaciondistrital.repository.impl;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

public class GeneralRepositoryImpl {
	@Autowired
	private DataSource dataSource;
	
//	@Autowired
//	private SessionFactory sessionFactory;	
//
	public DataSource getDataSource() {
		return dataSource;
	}
//
//	public void setDataSource(DataSource dataSource) {
//		this.dataSource = dataSource;
//	}
//	
//	protected Session getSession(){
//		return sessionFactory.getCurrentSession();
//	}	
	
    protected java.sql.Date convertUtilToSql(java.util.Date uDate) {
        java.sql.Date sDate = new java.sql.Date(uDate.getTime());
        return sDate;
    }
}
