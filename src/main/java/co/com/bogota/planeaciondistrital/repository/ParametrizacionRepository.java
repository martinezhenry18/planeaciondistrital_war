package co.com.bogota.planeaciondistrital.repository;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import co.com.bogota.planeaciondistrital.dto.ResponseGeneralDto;
import co.com.bogota.planeaciondistrital.dto.parametrizacion.ParametrizacionListasBasicasDto;
import co.com.bogota.planeaciondistrital.exception.PlaneacionException;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListaDetalleModel;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListasBasicasModel;

/**
	*	@author 	Henry Martinez
	*	@empresa	Ares Soluciones S.A.S
	*	@mail		hmartinez@aressoluciones.com
	*	@fecha 		22/05/2019
	*	@proyeco	Planeacion Distrital
	*	@clase 		ParametrizacionRepository.java
	*	@comments	
 */
@Repository
public interface ParametrizacionRepository
{
	public ArrayList<ParametrizacionListasBasicasModel> getListasBasicas() throws SQLException;
	
	public ArrayList<ParametrizacionListaDetalleModel> getListaParametroDetalle(int idLista) throws SQLException;
	
	public ArrayList<ParametrizacionListaDetalleModel> getListaParametroSemaforo(int idLista) throws SQLException;
	
	public ResponseGeneralDto guardarDetalleListaBasica(ParametrizacionListaDetalleModel modelParametrizacionListaDetalleModel) throws PlaneacionException, SQLException;
	
	public ResponseGeneralDto actualizarDetalleListaBasica(ParametrizacionListaDetalleModel modelParametrizacionListaDetalleModel) throws PlaneacionException, SQLException;
	
	public ResponseGeneralDto actualizarParametroListaBasica(ParametrizacionListasBasicasModel modelParametrizacionListasBasicasModel) throws PlaneacionException, SQLException;

}
