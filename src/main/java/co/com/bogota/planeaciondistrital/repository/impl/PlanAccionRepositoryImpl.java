package co.com.bogota.planeaciondistrital.repository.impl;

import co.com.bogota.planeaciondistrital.model.planAccion.*;
import co.com.bogota.planeaciondistrital.repository.PlanAccionRepository;
import oracle.jdbc.OracleTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class PlanAccionRepositoryImpl extends GeneralRepositoryImpl implements PlanAccionRepository {
    private static Logger logger = LoggerFactory.getLogger(PlanAccionRepositoryImpl.class);


    @Override
    public List<respuestaConsultaObjetivosModel> consultarObjetivo(String P_CODIGO_POLITICA) throws SQLException {
        Connection conn = getDataSource().getConnection();
        ResultSet rset;
        CallableStatement pStmt = null;
        pStmt = conn.prepareCall("{CALL SP_CONSULTA_OBJETIVO (?,?)}");
        pStmt.setObject(1, P_CODIGO_POLITICA);
        pStmt.registerOutParameter(2, OracleTypes.CURSOR);
        pStmt.execute();
        rset = (ResultSet) pStmt.getObject(2);
        List<respuestaConsultaObjetivosModel> ListaObjetivos = new ArrayList<>();

        while (rset.next()) {
            respuestaConsultaObjetivosModel Objetivos = new respuestaConsultaObjetivosModel();
            Objetivos.setCODIGO_OBJETIVO(rset.getString("CODIGO_OBJETIVO"));
            Objetivos.setOBJETIVO_GENERAL(rset.getString("OBJETIVO_GENERAL"));
            Objetivos.setCODIGO_OBJETIVO_ESPECIFICO(rset.getString("CODIGO_OBJETIVO_ESPECIFICO"));
            Objetivos.setOBJETIVO_ESPECIFICO(rset.getString("OBJETIVO_ESPECIFICO"));

            ListaObjetivos.add(Objetivos);
        }
        rset.close();

        return ListaObjetivos;
    }

    @Override
    public List<SectoresModel> ConsultarSectores() throws SQLException {
        Connection conn = getDataSource().getConnection();
        ResultSet rset;
        CallableStatement pStmt = null;
        pStmt = conn.prepareCall("{CALL SP_LISTA_SECTORES (?)}");
        pStmt.registerOutParameter(1, OracleTypes.CURSOR);
        pStmt.execute();
        rset = (ResultSet) pStmt.getObject(1);
        List<SectoresModel> Lista = new ArrayList<>();

        while (rset.next()) {
            Lista.add(new SectoresModel(rset.getString("NOMBRE"), rset.getString("CODIGO_PARAMETROS_SISTEMA")));
        }
        rset.close();

        return Lista;


    }

    @Override
    public List<EntidadesModel> ConsultarEntidades(String P_VALOR_SECTOR) throws SQLException {
        Connection conn = getDataSource().getConnection();
        ResultSet rset;
        CallableStatement pStmt = null;
        pStmt = conn.prepareCall("{CALL SP_LISTA_ENTIDADES (?,?)}");
        pStmt.setObject(1, P_VALOR_SECTOR);
        pStmt.registerOutParameter(2, OracleTypes.CURSOR);
        pStmt.execute();
        rset = (ResultSet) pStmt.getObject(2);
        List<EntidadesModel> Lista = new ArrayList<>();

        while (rset.next()) {
            Lista.add(new EntidadesModel(rset.getString("NOMBRE"), rset.getString("VALOR")));
        }
        rset.close();

        return Lista;


    }

    @Override
    public List<TipoAnualizacionModel> ConsultarTiposAnualizacion() throws SQLException {
        Connection conn = getDataSource().getConnection();
        ResultSet rset;
        CallableStatement pStmt = null;
        pStmt = conn.prepareCall("{CALL SP_LISTA_TIPO_ANUALIZACION (?)}");
        pStmt.registerOutParameter(1, OracleTypes.CURSOR);
        pStmt.execute();
        rset = (ResultSet) pStmt.getObject(1);
        List<TipoAnualizacionModel> Lista = new ArrayList<>();

        while (rset.next()) {
            Lista.add(new TipoAnualizacionModel(rset.getString("NOMBRE"), rset.getString("CODIGO_PARAMETROS_SISTEMA")));
        }
        rset.close();

        return Lista;


    }

    @Override
    public List<UnidadesMedidasModel> ConsultarUnidadesMedidas() throws SQLException {
        Connection conn = getDataSource().getConnection();
        ResultSet rset;
        CallableStatement pStmt = null;
        pStmt = conn.prepareCall("{CALL SP_LISTA_UNIDAD_MEDIDA (?)}");
        pStmt.registerOutParameter(1, OracleTypes.CURSOR);
        pStmt.execute();
        rset = (ResultSet) pStmt.getObject(1);
        List<UnidadesMedidasModel> Lista = new ArrayList<>();

        while (rset.next()) {
            Lista.add(new UnidadesMedidasModel(rset.getString("NOMBRE"), rset.getString("CODIGO_PARAMETROS_SISTEMA")));
        }
        rset.close();

        return Lista;
    }

    @Override
    public List<ConsultaAniosModel> ConsultarAnios(String P_CODIGO_INDICADOR) throws SQLException {
        Connection conn = getDataSource().getConnection();
        ResultSet rset;
        CallableStatement pStmt = null;
        pStmt = conn.prepareCall("{CALL SP_CONSULTA_ANIOS (?,?)}");
        pStmt.setObject(1, P_CODIGO_INDICADOR);
        pStmt.registerOutParameter(2, OracleTypes.CURSOR);
        pStmt.execute();
        rset = (ResultSet) pStmt.getObject(2);
        List<ConsultaAniosModel> Lista = new ArrayList<>();

        while (rset.next()) {
            Lista.add(new ConsultaAniosModel(rset.getInt("ANIOS"), rset.getInt("VALOR")));
        }
        rset.close();

        return Lista;
    }


    @Override
    public ResultadoModel InsertarResultado(ResultadoModel modelo) throws SQLException {
        Connection conn = getDataSource().getConnection();
        CallableStatement pStmt = null;
        pStmt = conn.prepareCall("{CALL SP_INSERTA_RESULTADO (?,?,?,?,?,?,?,?,?,?)}");
        pStmt.setObject(1, modelo.getCODIGO_POLITICA());

        if (Objects.isNull(modelo.getCODIGO_OBJETIVO())) {
            pStmt.setObject(2, OracleTypes.NUMBER);
        } else {
            pStmt.setObject(2, modelo.getCODIGO_OBJETIVO());
        }
        pStmt.setObject(3, modelo.getNOMBRE_RESULTADO());
        pStmt.setObject(4, modelo.getCODIGO_SECTOR());
        pStmt.setObject(5, modelo.getCODIGO_ENTIDAD());
        if (Objects.isNull(modelo.getIMPORTANCIA_RELATIVA())) {
            pStmt.setObject(6, OracleTypes.NUMBER);
        } else {
            pStmt.setObject(6, modelo.getIMPORTANCIA_RELATIVA());
        }
        pStmt.setObject(7, modelo.getUSUARIO());
        pStmt.registerOutParameter(8, OracleTypes.VARCHAR);
        pStmt.registerOutParameter(9, OracleTypes.VARCHAR);
        pStmt.registerOutParameter(10, OracleTypes.INTEGER);
        pStmt.execute();
        modelo.setCODIGO_RESULTADO(pStmt.getString(8));
        modelo.setMENSAJE(pStmt.getString(9));
        modelo.setVALIDACION(pStmt.getInt(10));
        return modelo;


    }


    @Override
    public IndicadorModel InsertarIndicador(IndicadorModel modelo) throws SQLException {
        Connection conn = getDataSource().getConnection();
        CallableStatement pStmt = null;
        pStmt = conn.prepareCall("{CALL SP_INSERTA_INDICADOR (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
        pStmt.setObject(1, modelo.getTIPO_CODIGO());
        pStmt.setObject(2, modelo.getNOMBRE_INDICADOR());
        pStmt.setObject(3, modelo.getFORMULA_INDICADOR());
        pStmt.setObject(4, modelo.getINDICADOR_PDD());
        pStmt.setObject(5, new Date(modelo.getTIEMPO_EJECUCION_INICIO().getTime()));
        pStmt.setObject(6, new Date(modelo.getTIEMPO_EJECUCION_FIN().getTime()));
        pStmt.setObject(7, modelo.getLINEA_BASE_VALOR());
        pStmt.setObject(8, new Date(modelo.getLINEA_BASE_FECHA().getTime()));
        pStmt.setObject(9, modelo.getLINEA_BASE_FUENTE());
        pStmt.setObject(10, modelo.getCODIGO_RESULTADO());
        pStmt.setObject(11, modelo.getUNIDAD_MEDIDA());
        pStmt.setObject(12, modelo.getTIPO_ANUALIZACION());
        pStmt.setObject(13, modelo.getCODIGO_INDICADOR_PDD());
        pStmt.setObject(14, modelo.getUSUARIO());
        pStmt.registerOutParameter(15, OracleTypes.VARCHAR);
        pStmt.registerOutParameter(16, OracleTypes.VARCHAR);
        pStmt.registerOutParameter(17, OracleTypes.INTEGER);
        pStmt.execute();
        modelo.setCODIGO_INDICADOR(pStmt.getString(15));
        modelo.setMENSAJE(pStmt.getString(16));
        modelo.setVALIDACION(pStmt.getInt(17));
        return modelo;


    }

    @Override
    public MetaModel InsertarMeta(MetaModel modelo) throws SQLException {
        Connection conn = getDataSource().getConnection();
        CallableStatement pStmt = null;
        pStmt = conn.prepareCall("{CALL SP_INSERTA_META (?,?,?,?,?,?)}");
        if (Objects.isNull(modelo.getANIO())) {
            pStmt.setObject(1, OracleTypes.NUMBER);
        } else {
            pStmt.setObject(1, modelo.getANIO());
        }
        if (Objects.isNull(modelo.getANIO())) {
            pStmt.setObject(2, OracleTypes.NUMBER);
        } else {
            pStmt.setObject(2, modelo.getVALOR());
        }
        pStmt.setObject(3, modelo.getCODIGO_INDICADOR());
        pStmt.setObject(4, modelo.getUSUARIO_CREACION());
        pStmt.registerOutParameter(5, OracleTypes.VARCHAR);
        pStmt.registerOutParameter(6, OracleTypes.INTEGER);
        pStmt.execute();
        modelo.setMENSAJE(pStmt.getString(5));
        modelo.setVALIDACION(pStmt.getInt(6));
        return modelo;

    }

    @Override
    public ProductoModel InsertarProducto(ProductoModel modelo) throws SQLException {
        Connection conn = getDataSource().getConnection();
        CallableStatement pStmt = null;
        pStmt = conn.prepareCall("{CALL SP_INSERTA_PRODUCTO (?,?,?,?,?,?,?,?)}");
        pStmt.setObject(1, modelo.getNOMBRE_PRODUCTO());
        if (Objects.isNull(modelo.getIMPORTANCIA_RELATIVA())) {
            pStmt.setObject(2, OracleTypes.NUMBER);
        } else {
            pStmt.setObject(2, modelo.getIMPORTANCIA_RELATIVA());
        }
        pStmt.setObject(3, modelo.getCODIGO_SECTOR());
        pStmt.setObject(4, modelo.getCODIGO_ENTIDAD());
        pStmt.setObject(5, modelo.getCODIGO_RESULTADO());
        pStmt.setObject(6, modelo.getUSUARIO());
        pStmt.registerOutParameter(7, OracleTypes.VARCHAR);
        pStmt.registerOutParameter(8, OracleTypes.INTEGER);
        pStmt.execute();
        modelo.setMENSAJE(pStmt.getString(7));
        modelo.setVALIDACION(pStmt.getInt(8));
        return modelo;

    }


}
