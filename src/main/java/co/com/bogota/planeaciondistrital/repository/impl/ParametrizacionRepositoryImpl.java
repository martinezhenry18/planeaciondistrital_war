package co.com.bogota.planeaciondistrital.repository.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import co.com.bogota.planeaciondistrital.dto.ResponseGeneralDto;
import co.com.bogota.planeaciondistrital.dto.parametrizacion.ParametrizacionListasBasicasDto;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListaDetalleModel;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListasBasicasModel;
import co.com.bogota.planeaciondistrital.repository.ParametrizacionRepository;

/**
	*	@author 	Henry Martinez
	*	@empresa	Ares Soluciones S.A.S
	*	@mail		hmartinez@aressoluciones.com
	*	@fecha 		22/05/2019
	*	@proyeco	Planeacion Distrital
	*	@clase 		ParametrizacionRepositoryImpl.java
	*	@comments	
 */

@Repository
public class ParametrizacionRepositoryImpl extends GeneralRepositoryImpl implements ParametrizacionRepository
{
	
	
	private static Logger logger = LoggerFactory.getLogger(ParametrizacionRepositoryImpl.class);
    
    private ResultSet rset = null;
    private Connection conn;
    private CallableStatement psmt = null;
    
    
    private ResponseGeneralDto objResponseGeneralDto;
    
    

	
	@Override
	public ArrayList<ParametrizacionListasBasicasModel> getListasBasicas() throws SQLException
	{
		conn = getDataSource().getConnection();
		ArrayList<ParametrizacionListasBasicasModel> cursor = new ArrayList<ParametrizacionListasBasicasModel>();
		ParametrizacionListasBasicasModel objParametrizacionListasBasicasModel = null;
		
		psmt = conn.prepareCall("{call SP_CONSULTA_PARAMETROS (?)}");
		psmt.registerOutParameter(1, oracle.jdbc.OracleTypes.CURSOR);
		psmt.execute();
		
		rset = (ResultSet) psmt.getObject(1);
		
		while (rset.next())
		{
			objParametrizacionListasBasicasModel = new ParametrizacionListasBasicasModel();
			objParametrizacionListasBasicasModel.setEstadoLista(rset.getString("ESTADO"));
			objParametrizacionListasBasicasModel.setIdLista(Integer.parseInt(rset.getString("CODIGO_PARAMETRO")));
			objParametrizacionListasBasicasModel.setNombreLista(rset.getString("NOMBRE"));
			objParametrizacionListasBasicasModel.setDescripcionLista(rset.getString("DESCRIPCION"));
			objParametrizacionListasBasicasModel.setValorLista(rset.getString("VALOR"));
			objParametrizacionListasBasicasModel.setTipoParametroLista(rset.getInt("TIPO_PARAMETRO"));

			cursor.add(objParametrizacionListasBasicasModel);
		}
			
		
		conn.close();
		
		return cursor;

	}
	
	
	@Override
	public ArrayList<ParametrizacionListaDetalleModel> getListaParametroDetalle(int idLista) throws SQLException
	{
		conn = getDataSource().getConnection();
		ArrayList<ParametrizacionListaDetalleModel> cursor = new ArrayList<ParametrizacionListaDetalleModel>();
		ParametrizacionListaDetalleModel objParametrizacionListaDetalleModel = null;
		
		
		psmt = conn.prepareCall("{call SP_CONSULTA_PARAMETROS_SIS (? , ?)}");
		psmt.setInt(1, idLista);
		psmt.registerOutParameter(2, oracle.jdbc.OracleTypes.CURSOR);
		psmt.execute();
		
		rset = (ResultSet) psmt.getObject(2);
		
		while (rset.next())
		{
			objParametrizacionListaDetalleModel = new ParametrizacionListaDetalleModel();
			objParametrizacionListaDetalleModel.setDescripcionParametro(rset.getString("DESCRIPCION"));
			objParametrizacionListaDetalleModel.setEstadoParametro(rset.getString("ESTADO"));
			objParametrizacionListaDetalleModel.setNombreParametro(rset.getString("NOMBRE"));
			objParametrizacionListaDetalleModel.setIdParametro(Integer.parseInt(rset.getString("CODIGO_PARAMETROS_SISTEMA")));
			objParametrizacionListaDetalleModel.setNombreParametroPadre(rset.getString("NOMBRE_PADRE"));

			cursor.add(objParametrizacionListaDetalleModel);
		}
			
		
		conn.close();
		
		return cursor;

	}
	
	@Override
	public ArrayList<ParametrizacionListaDetalleModel> getListaParametroSemaforo(int idLista) throws SQLException
	{
		conn = getDataSource().getConnection();
		ArrayList<ParametrizacionListaDetalleModel> cursor = new ArrayList<ParametrizacionListaDetalleModel>();
		ParametrizacionListaDetalleModel objParametrizacionListaDetalleModel = null;
		
		
		psmt = conn.prepareCall("{call SP_CONSULTA_RANGO_SEMAFORO (?)}");
		psmt.registerOutParameter(1, oracle.jdbc.OracleTypes.CURSOR);
		psmt.execute();
		
		rset = (ResultSet) psmt.getObject(1);
		
		while (rset.next())
		{
			objParametrizacionListaDetalleModel = new ParametrizacionListaDetalleModel();
			objParametrizacionListaDetalleModel.setDescripcionParametro(rset.getString("DESCRIPCION"));
			objParametrizacionListaDetalleModel.setNombreParametro(rset.getString("NOMBRE"));
			objParametrizacionListaDetalleModel.setIdParametro(Integer.parseInt(rset.getString("ID_RANGO")));
			objParametrizacionListaDetalleModel.setColorRangoSemaforo(rset.getString("COLOR"));
			objParametrizacionListaDetalleModel.setDesdeRangoSemaforo(rset.getDouble("DESDE"));
			objParametrizacionListaDetalleModel.setHastaRangoSemaforo(rset.getDouble("HASTA"));

			cursor.add(objParametrizacionListaDetalleModel);
		}
			
		
		conn.close();
		
		return cursor;

	}
	
	
	@Override
	public ResponseGeneralDto guardarDetalleListaBasica(ParametrizacionListaDetalleModel modelParametrizacionListaDetalleModel) throws SQLException
	{
		
		conn = getDataSource().getConnection();
		
		psmt = conn.prepareCall("{call SP_INSERTA_PARAMETRO_SIS (?, ?, ?, ?, ?, ?, ?)}");
		psmt.setString(1, modelParametrizacionListaDetalleModel.getNombreParametro());
		psmt.setString(2, modelParametrizacionListaDetalleModel.getDescripcionParametro());
		psmt.setNString(3, modelParametrizacionListaDetalleModel.getEstadoParametro());
		psmt.setInt(4, modelParametrizacionListaDetalleModel.getIdParametroListaBasica());
		psmt.setInt(5, modelParametrizacionListaDetalleModel.getIdParametroPadre());
		
		psmt.registerOutParameter(6, oracle.jdbc.OracleTypes.VARCHAR);
		psmt.registerOutParameter(7, oracle.jdbc.OracleTypes.INTEGER);
		
		psmt.execute();
		
		objResponseGeneralDto = new ResponseGeneralDto();

		objResponseGeneralDto.setMensajeResponse(psmt.getString(6));
		objResponseGeneralDto.setIdResponse(Integer.toString(psmt.getInt(7)));
		
		return objResponseGeneralDto;
	}
	
	@Override
	public ResponseGeneralDto actualizarDetalleListaBasica(ParametrizacionListaDetalleModel modelParametrizacionListaDetalleModel) throws SQLException
	{
		
		conn = getDataSource().getConnection();
		
		psmt = conn.prepareCall("{call SP_ACTUALIZA_PARAMETROS_SIS (?, ?, ?, ?, ?, ?, ?, ?)}");
		psmt.setInt(1, modelParametrizacionListaDetalleModel.getIdParametro());
		psmt.setString(2, modelParametrizacionListaDetalleModel.getNombreParametro());
		psmt.setString(3, modelParametrizacionListaDetalleModel.getDescripcionParametro());
		psmt.setNString(4, modelParametrizacionListaDetalleModel.getEstadoParametro());
		psmt.setInt(5, modelParametrizacionListaDetalleModel.getIdParametroListaBasica());
		psmt.setInt(6, modelParametrizacionListaDetalleModel.getIdParametroPadre());
		
		psmt.registerOutParameter(7, oracle.jdbc.OracleTypes.VARCHAR);
		psmt.registerOutParameter(8, oracle.jdbc.OracleTypes.INTEGER);
		
		psmt.execute();
		
		objResponseGeneralDto = new ResponseGeneralDto();

		objResponseGeneralDto.setMensajeResponse(psmt.getString(7));
		objResponseGeneralDto.setIdResponse(Integer.toString(psmt.getInt(8)));
		
		return objResponseGeneralDto;
	}
	
	@Override
	public ResponseGeneralDto actualizarParametroListaBasica(ParametrizacionListasBasicasModel modelParametrizacionListasBasicasModel) throws SQLException
	{
		
		conn = getDataSource().getConnection();
		
		psmt = conn.prepareCall("{call SP_ACTUALIZA_PARAMETRO (?, ?, ?, ?, ?)}");
		psmt.setInt(1, modelParametrizacionListasBasicasModel.getIdLista());
		psmt.setString(2, "HENRY MARTINEZ");
		psmt.setString(3, modelParametrizacionListasBasicasModel.getValorLista());
		
		psmt.registerOutParameter(4, oracle.jdbc.OracleTypes.VARCHAR);
		psmt.registerOutParameter(5, oracle.jdbc.OracleTypes.INTEGER);
		
		psmt.execute();
		
		objResponseGeneralDto = new ResponseGeneralDto();

		objResponseGeneralDto.setMensajeResponse(psmt.getString(4));
		objResponseGeneralDto.setIdResponse(Integer.toString(psmt.getInt(5)));
		
		return objResponseGeneralDto;
	}


	public ResponseGeneralDto getObjResponseGeneralDto()
	{
		return objResponseGeneralDto;
	}


	public void setObjResponseGeneralDto(ResponseGeneralDto objResponseGeneralDto)
	{
		this.objResponseGeneralDto = objResponseGeneralDto;
	}
}
