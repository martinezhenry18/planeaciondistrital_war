package co.com.bogota.planeaciondistrital.repository.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import co.com.bogota.planeaciondistrital.Constants;
import co.com.bogota.planeaciondistrital.exception.PlaneacionException;
import co.com.bogota.planeaciondistrital.model.PoliticaPublicaDatosGeneralesModel;
import co.com.bogota.planeaciondistrital.repository.PoliticaPublicaRepository;
import oracle.jdbc.OracleTypes;

/**
 * 
 * @author Daniel Arbey Sarmiento Amaya
 *
 */
@Repository
public class PoliticaPublicaRepositoryImpl extends GeneralRepositoryImpl implements PoliticaPublicaRepository{
	private static Logger logger = LoggerFactory.getLogger(PoliticaPublicaRepositoryImpl.class);
	
	/**
	 * @author Daniel Arbey Sarmiento Amaya
	 * @param politicaPublicaDatosGeneralesModel
	 * @throws PlaneacionException
	 */
	@Override
	public void guardarDatosGenerales(PoliticaPublicaDatosGeneralesModel politicaPublicaDatosGeneralesModel)
			throws PlaneacionException {
		
		logger.info("Guardar Datos Generales");
		Integer response = null;

		try {
			Connection conn = getDataSource().getConnection();
			CallableStatement pStmt = null;
			pStmt = conn.prepareCall("{call SP_INSERTA_POLITICA_PUBLICA(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
			
			/*
			 */
			
			pStmt.setObject(1, politicaPublicaDatosGeneralesModel.getNombrePolitica());	
			if(Objects.isNull(politicaPublicaDatosGeneralesModel.getNemotDocumento())) {
				pStmt.setNull(2, OracleTypes.NUMBER);
			}else {
				pStmt.setObject(2, politicaPublicaDatosGeneralesModel.getNemotDocumento());
			}
			pStmt.setObject(3, politicaPublicaDatosGeneralesModel.getAnno());
			pStmt.setDate(4, convertUtilToSql(politicaPublicaDatosGeneralesModel.getFechaInicio()));
			pStmt.setDate(5, convertUtilToSql(politicaPublicaDatosGeneralesModel.getFechaFin()));
			pStmt.setObject(6, politicaPublicaDatosGeneralesModel.getCostoTotalPolitica());
			pStmt.setObject(7, politicaPublicaDatosGeneralesModel.getSectorLider());
			pStmt.setObject(8, politicaPublicaDatosGeneralesModel.getEntidadLider());
			pStmt.setObject(9, "usuario");
			pStmt.setObject(10, "rutaimagen");
			pStmt.registerOutParameter(11, OracleTypes.VARCHAR);
			pStmt.registerOutParameter(12, OracleTypes.VARCHAR);
			pStmt.registerOutParameter(13, OracleTypes.INTEGER);
			
			pStmt.executeUpdate();
			response = (Integer)pStmt.getObject(13);
			pStmt.close();
			if(response==0) {
				throw new PlaneacionException("0"+Constants.CHARTER_SPLIT+"ERROR AL INSERTAR EL REGISTRO");
			} 
		} catch (SQLException e) {
			logger.error("Error: {}",e.getMessage());
			throw new PlaneacionException("Error en el sistema");
		} 
		
	}

}
