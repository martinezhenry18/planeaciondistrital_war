package co.com.bogota.planeaciondistrital.exception;

/**
 * 
 * @author Daniel Arbey Sarmiento Amaya
 *
 */
public class PlaneacionException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4004531688790766618L;
	
	public PlaneacionException() {
		super();
	}
	
	public PlaneacionException(String message) {
		super(message);
	}

}
