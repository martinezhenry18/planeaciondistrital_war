package co.com.bogota.planeaciondistrital.component;

import co.com.bogota.planeaciondistrital.dto.parametrizacion.ParametrizacionListaDetalleDto;
import co.com.bogota.planeaciondistrital.dto.parametrizacion.ParametrizacionListasBasicasDto;
import co.com.bogota.planeaciondistrital.dto.planAccion.*;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListaDetalleModel;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListasBasicasModel;
import co.com.bogota.planeaciondistrital.model.planAccion.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ConvertModelDto {

    @Autowired
    private ModelMapper modelMapper;

    /**
     * @param request
     * @return
     * @throws ParseException
     */
    public List<ObjetivoGeneralDto> convertRespuestaConsultaObjetivoModel(
            List<respuestaConsultaObjetivosModel> request) throws ParseException {
        List<ObjetivoGeneralDto> listdtop = new ArrayList<>();
        for (respuestaConsultaObjetivosModel model : request) {
            ObjetivoGeneralDto dto = modelMapper.map(model, ObjetivoGeneralDto.class);
            if (!listdtop.stream().filter(objetivodto -> dto.getCODIGO_OBJETIVO().equals(objetivodto.getCODIGO_OBJETIVO())).findFirst().isPresent()) {
                List<respuestaConsultaObjetivosModel> listaEspecificos = request.stream().filter(objetivosespecificos -> dto.getCODIGO_OBJETIVO().equals(objetivosespecificos.getCODIGO_OBJETIVO())).collect(Collectors.toList());
                dto.setOBJETIVOS_ESPECIFICOS(new ArrayList<>());
                for (respuestaConsultaObjetivosModel ObjetivosEspecifcos : listaEspecificos) {
                    dto.getOBJETIVOS_ESPECIFICOS().add(new ObjetivoEspecificoDto(ObjetivosEspecifcos.getCODIGO_OBJETIVO_ESPECIFICO(), ObjetivosEspecifcos.getOBJETIVO_ESPECIFICO()));

                }
                listdtop.add(dto);
            }

        }
        return listdtop;
    }

    /**
     * @param request
     * @return
     * @throws ParseException
     */
    public List<SectoresDto> convertSectoresModel(
            List<SectoresModel> request) throws ParseException {
        List<SectoresDto> listdtop = new ArrayList<>();
        for (SectoresModel model : request) {
            SectoresDto dto = modelMapper.map(model, SectoresDto.class);
            listdtop.add(dto);

        }
        return listdtop;
    }

    /**
     * @param request
     * @return
     * @throws ParseException
     */
    public List<TipoAnualizacionDto> convertTiposAnualizacionesModel(
            List<TipoAnualizacionModel> request) throws ParseException {
        List<TipoAnualizacionDto> listdtop = new ArrayList<>();
        for (TipoAnualizacionModel model : request) {
            TipoAnualizacionDto dto = modelMapper.map(model, TipoAnualizacionDto.class);
            listdtop.add(dto);

        }
        return listdtop;
    }

    /**
     * @param request
     * @return
     * @throws ParseException
     */
    public List<UnidadesMedidasDto> convertUnidadesMedidasModel(
            List<UnidadesMedidasModel> request) throws ParseException {
        List<UnidadesMedidasDto> listdtop = new ArrayList<>();
        for (UnidadesMedidasModel model : request) {
            UnidadesMedidasDto dto = modelMapper.map(model, UnidadesMedidasDto.class);
            listdtop.add(dto);

        }
        return listdtop;
    }

    /**
     * @param request
     * @return
     * @throws ParseException
     */
    public List<EntidadesDto> convertEntidadesModel(
            List<EntidadesModel> request) throws ParseException {
        List<EntidadesDto> listdtop = new ArrayList<>();
        for (EntidadesModel model : request) {
            EntidadesDto dto = modelMapper.map(model, EntidadesDto.class);
            listdtop.add(dto);

        }
        return listdtop;
    }

    public ResultadoDto convertResultadoModel(
            ResultadoModel request) throws ParseException {

        ResultadoDto dto = modelMapper.map(request, ResultadoDto.class);

        return dto;
    }


    public ParametrizacionListaDetalleDto convertListaDetalleDto(ParametrizacionListaDetalleModel model) throws ParseException
    {
        ParametrizacionListaDetalleDto objParametrizacionListaDetalleDto = modelMapper.map(model, ParametrizacionListaDetalleDto.class);
        return objParametrizacionListaDetalleDto;
    }


    public ParametrizacionListasBasicasDto convertListaBasicaDto(ParametrizacionListasBasicasModel model) throws ParseException
    {
        ParametrizacionListasBasicasDto objParametrizacionListasBasicasDto = modelMapper.map(model, ParametrizacionListasBasicasDto.class);
        return objParametrizacionListasBasicasDto;
    }

    public IndicadorDto convertIndicadoresModel(
            IndicadorModel request) throws ParseException {

        IndicadorDto dto = modelMapper.map(request, IndicadorDto.class);

        return dto;
    }

    public MetaDto convertMetaModel(
            MetaModel request) throws ParseException {

        MetaDto dto = modelMapper.map(request, MetaDto.class);

        return dto;
    }

    public ProductoDto convertProductoModel(
            ProductoModel request) throws ParseException {

        ProductoDto dto = modelMapper.map(request, ProductoDto.class);

        return dto;
    }

    public List<ConsultaAniosDto> convertAniosModel(
            List<ConsultaAniosModel> request) throws ParseException {
        List<ConsultaAniosDto> listdtop = new ArrayList<>();
        for (ConsultaAniosModel model : request) {
            ConsultaAniosDto dto = modelMapper.map(model, ConsultaAniosDto.class);
            listdtop.add(dto);
        }
        return listdtop;
    }

}
