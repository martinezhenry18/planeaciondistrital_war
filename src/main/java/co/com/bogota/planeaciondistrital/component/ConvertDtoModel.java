package co.com.bogota.planeaciondistrital.component;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import co.com.bogota.planeaciondistrital.dto.planAccion.IndicadorDto;
import co.com.bogota.planeaciondistrital.dto.planAccion.MetaDto;
import co.com.bogota.planeaciondistrital.dto.planAccion.ProductoDto;
import co.com.bogota.planeaciondistrital.dto.planAccion.ResultadoDto;
import co.com.bogota.planeaciondistrital.model.planAccion.IndicadorModel;
import co.com.bogota.planeaciondistrital.model.planAccion.MetaModel;
import co.com.bogota.planeaciondistrital.model.planAccion.ProductoModel;
import co.com.bogota.planeaciondistrital.model.planAccion.ResultadoModel;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import co.com.bogota.planeaciondistrital.dto.PoliticaPublicaDatosGeneralesDto;
import co.com.bogota.planeaciondistrital.dto.parametrizacion.ParametrizacionListaDetalleDto;
import co.com.bogota.planeaciondistrital.dto.parametrizacion.ParametrizacionListasBasicasDto;
import co.com.bogota.planeaciondistrital.model.PoliticaPublicaDatosGeneralesModel;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListaDetalleModel;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListasBasicasModel;

/**
 * @author Daniel Arbey Sarmiento Amaya
 * @see Encargado de trasformar dto a model
 */
@Component
public class ConvertDtoModel {

    @Autowired
    private ModelMapper modelMapper;

    /**
     * @param request
     * @return
     * @throws ParseException
     */
    public PoliticaPublicaDatosGeneralesModel convertPoliticaPublicaDatosGenerales(
            PoliticaPublicaDatosGeneralesDto request) throws ParseException {
        dateToString();
        PoliticaPublicaDatosGeneralesModel model = modelMapper.map(request, PoliticaPublicaDatosGeneralesModel.class);
        return model;
    }

    public IndicadorModel convertIndicadorDto(
            IndicadorDto request) throws ParseException {
        System.out.println("fecha: "+ request.getTIEMPO_EJECUCION_INICIO());
        IndicadorModel model = modelMapper.map(request, IndicadorModel.class);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        if (Objects.isNull(request.getTIEMPO_EJECUCION_INICIO())) {
            model.setTIEMPO_EJECUCION_INICIO(null);
        } else {
            System.out.println("getTIEMPO_EJECUCION_INICIO: "+request.getTIEMPO_EJECUCION_INICIO());
            Date dateIni = formatter.parse(request.getTIEMPO_EJECUCION_INICIO());
            model.setTIEMPO_EJECUCION_INICIO(dateIni);
        }
        if (Objects.isNull(request.getTIEMPO_EJECUCION_FIN())) {
            model.setTIEMPO_EJECUCION_FIN(null);
        } else {
            System.out.println("getTIEMPO_EJECUCION_FIN: "+request.getTIEMPO_EJECUCION_FIN());
            Date dateFin = formatter.parse(request.getTIEMPO_EJECUCION_FIN());
            model.setTIEMPO_EJECUCION_FIN(dateFin);
        }
        if (Objects.isNull(request.getLINEA_BASE_FECHA())) {
            model.setLINEA_BASE_FECHA(null);
        } else {
            System.out.println("getLINEA_BASE_FECHA: "+request.getLINEA_BASE_FECHA());
            Date dateAprobacion = formatter.parse(request.getLINEA_BASE_FECHA());
            model.setLINEA_BASE_FECHA(dateAprobacion);
        }
        return model;
    }

    public ResultadoModel convertResultadoDto(
            ResultadoDto request) throws ParseException {

        ResultadoModel model = modelMapper.map(request, ResultadoModel.class);
        return model;
    }

    public MetaModel convertMetaDto(
            MetaDto request) throws ParseException {

        MetaModel model = modelMapper.map(request, MetaModel.class);
        return model;
    }

    public ProductoModel convertProductoDto(
            ProductoDto request) throws ParseException {

        ProductoModel model = modelMapper.map(request, ProductoModel.class);
        return model;
    }

	
	public ParametrizacionListaDetalleModel convertParametrizacionListaDetalle(
			ParametrizacionListaDetalleDto request) throws ParseException {
		
		ParametrizacionListaDetalleModel model = modelMapper.map(request, ParametrizacionListaDetalleModel.class);
				
		if (Objects.isNull(request.getDescripcionParametro()))
		{
			model.setDescripcionParametro(null);
		} else {
			model.setDescripcionParametro(request.getDescripcionParametro());
		}
		
		if(Objects.isNull(request.getNombreParametro()))
		{
			model.setNombreParametro(null);
		} else {
			model.setNombreParametro(request.getNombreParametro());
		}
		
		if(Objects.isNull(request.getNombreParametroPadre()))
		{
			model.setNombreParametroPadre(null);
		} else {
			model.setNombreParametroPadre(request.getNombreParametroPadre());
		}
		
		if (Objects.isNull(request.getEstadoParametro()) || request.getEstadoParametro().equals("0"))
		{
			model.setEstadoParametro("0");
		} else {
			model.setEstadoParametro("1");
		}
		
		if (Objects.isNull(request.getDesdeRangoSemaforo()))
		{
			model.setColorRangoSemaforo(null);
			model.setDesdeRangoSemaforo(null);
			model.setHastaRangoSemaforo(null);
		} else {
			model.setColorRangoSemaforo(request.getColorRangoSemaforo());
			model.setDesdeRangoSemaforo(request.getDesdeRangoSemaforo());
			model.setHastaRangoSemaforo(request.getHastaRangoSemaforo());
		}
				
    	
        return model;
	}
	
	
	public ParametrizacionListasBasicasModel convertParametrizacionListaBasicaDto(
			ParametrizacionListasBasicasDto request) throws ParseException {
		
		ParametrizacionListasBasicasModel model = modelMapper.map(request, ParametrizacionListasBasicasModel.class);
				
		if (Objects.isNull(request.getDescripcionLista()))
		{
			model.setDescripcionLista(null);
		} else {
			model.setDescripcionLista(request.getDescripcionLista());
		}
		
		if(Objects.isNull(request.getNombreLista()))
		{
			model.setNombreLista(null);
		} else {
			model.setNombreLista(request.getNombreLista());
		}
		
		if(Objects.isNull(request.getValorLista()))
		{
			model.setValorLista(null);
		} else {
			model.setValorLista(request.getValorLista());
		}
		
		if (Objects.isNull(request.getEstadoLista()) || request.getEstadoLista().equals("0"))
		{
			model.setEstadoLista("0");
		} else {
			model.setEstadoLista("1");
		}
		
		if (Objects.isNull(request.getIdLista()))
		{
			model.setIdLista(null);
		} else {
			model.setIdLista(Integer.parseInt(request.getIdLista()));
		}
		
		if (Objects.isNull(request.getTipoParametroLista()))
		{
			model.setTipoParametroLista(null);
		} else {
			model.setTipoParametroLista(request.getTipoParametroLista());
		}
		
				
    	
        return model;
	}
	
	

    private void dateToString() {
    }

}
