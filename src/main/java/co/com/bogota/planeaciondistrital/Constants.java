package co.com.bogota.planeaciondistrital;

/**
 * 
 * @author Daniel Arbey Sarmiento Amaya
 *
 */
public class Constants {
	public static final Integer OK_INFO = 0;
	public static final Integer EER_INFO = -1;
	public static final Integer EER_DATA = -2;
	public static final String CHARTER_SPLIT="&";
	
}
