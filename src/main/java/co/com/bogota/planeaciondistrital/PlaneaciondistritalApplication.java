package co.com.bogota.planeaciondistrital;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.AbstractProvider;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PlaneaciondistritalApplication extends SpringBootServletInitializer{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PlaneaciondistritalApplication.class);
    }
    
	public static void main(String[] args) {
		SpringApplication.run(PlaneaciondistritalApplication.class, args);
	}

    @Bean
    public ModelMapper modelMapper() {
    	ModelMapper modelMapper = new ModelMapper();
	    Provider<Date> localDateProvider = new AbstractProvider<Date>() {
	        @Override
	        public Date get() {
	            return new Date();
	        }
	    };

	    Converter<String, Date> toStringDate = new AbstractConverter<String, Date>() {
	        @Override
	        protected Date convert(String source) {
	    		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	    		Date date;
				try {
					date = formatter.parse(source);
					return date;
				} catch (ParseException e) {
					return null;
				}
	        }
	    };


	    modelMapper.createTypeMap(String.class, Date.class);
	    modelMapper.addConverter(toStringDate);
	    modelMapper.getTypeMap(String.class, Date.class).setProvider(localDateProvider);    	
        return modelMapper;
    }  
}
