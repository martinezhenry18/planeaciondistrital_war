package co.com.bogota.planeaciondistrital.service;

import co.com.bogota.planeaciondistrital.exception.PlaneacionException;
import co.com.bogota.planeaciondistrital.model.PoliticaPublicaDatosGeneralesModel;

/**
 * 
 * @author Daniel Arbey Sarmiento Amaya
 * @see Gestor de politica pulbica
 *
 */
public interface PoliticaPublicaService {

	/**
	 * @see gestiona el guardado de los datos generales
	 * @param politicaPublicaDatosGeneralesModel
	 * @throws PlaneacionException
	 */
	void guardarDatosGenerales(PoliticaPublicaDatosGeneralesModel politicaPublicaDatosGeneralesModel) throws PlaneacionException;

}
