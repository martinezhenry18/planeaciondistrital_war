package co.com.bogota.planeaciondistrital.service;

import co.com.bogota.planeaciondistrital.exception.PlaneacionException;
import co.com.bogota.planeaciondistrital.model.PoliticaPublicaDatosGeneralesModel;
import co.com.bogota.planeaciondistrital.model.planAccion.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Adan Quintero
 * @see Gestor Plan de Accion
 */
public interface PlanAccionService {

    List<respuestaConsultaObjetivosModel> consultarObjetivo(String P_CODIGO_POLITICA) throws PlaneacionException, SQLException;

    List<SectoresModel> ConsultarSectores() throws SQLException;

    List<EntidadesModel> ConsultarEntidades(String P_VALOR_SECTOR) throws SQLException;

    List<TipoAnualizacionModel> ConsultarTiposAnualizacion() throws SQLException;

    List<UnidadesMedidasModel> ConsultarUnidadesMedidas() throws SQLException;

    List<ConsultaAniosModel> ConsultarAnios(String P_CODIGO_INDICADOR) throws SQLException;

    ResultadoModel InsertarResultado(ResultadoModel modelo) throws SQLException;

    IndicadorModel InsertarIndicador(IndicadorModel modelo) throws SQLException;

    MetaModel InsertarMeta(MetaModel modelo) throws SQLException;

    ProductoModel InsertarProducto(ProductoModel modelo) throws SQLException;



}
