package co.com.bogota.planeaciondistrital.service;

import java.sql.SQLException;
import java.util.ArrayList;

import co.com.bogota.planeaciondistrital.dto.ResponseGeneralDto;
import co.com.bogota.planeaciondistrital.exception.PlaneacionException;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListaDetalleModel;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListasBasicasModel;

/**
*	@author 	Henry Martinez
*	@empresa	Ares Soluciones S.A.S
*	@mail		hmartinez@aressoluciones.com
*	@fecha 		20/05/2019
*	@proyecto	Planeacion Distrital
*	@Interfaz 	ParametrizacionService.java
*	@comments	
*/





public interface ParametrizacionService
{

	public ArrayList<ParametrizacionListasBasicasModel> getListasBasicas() throws SQLException;
	
	public ArrayList<ParametrizacionListaDetalleModel> getListaParametroDetalle(int idLista) throws SQLException;
	
	public ResponseGeneralDto guardarDetalleListaBasica(ParametrizacionListaDetalleModel modelParametrizacionListaDetalleModel) throws PlaneacionException, SQLException;
	
	public ResponseGeneralDto actualizarDetalleListaBasica(ParametrizacionListaDetalleModel modelParametrizacionListaDetalleModel) throws PlaneacionException, SQLException;
	
	public ResponseGeneralDto actualizarParametroListaBasica(ParametrizacionListasBasicasModel modelParametrizacionListasBasicasModel) throws PlaneacionException, SQLException;
	
	

}
