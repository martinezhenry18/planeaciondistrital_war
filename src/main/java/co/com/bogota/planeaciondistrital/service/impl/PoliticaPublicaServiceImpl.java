package co.com.bogota.planeaciondistrital.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.bogota.planeaciondistrital.exception.PlaneacionException;
import co.com.bogota.planeaciondistrital.model.PoliticaPublicaDatosGeneralesModel;
import co.com.bogota.planeaciondistrital.repository.PoliticaPublicaRepository;
import co.com.bogota.planeaciondistrital.service.PoliticaPublicaService;

/**
 * 
 * @author Daniel Arbey Sarmiento Amaya
 *
 */
@Service
public class PoliticaPublicaServiceImpl implements PoliticaPublicaService{

	@Autowired
	private PoliticaPublicaRepository politicaPublicaRepository;
	
	@Override
	public void guardarDatosGenerales(PoliticaPublicaDatosGeneralesModel politicaPublicaDatosGeneralesModel)
			throws PlaneacionException {
		politicaPublicaRepository.guardarDatosGenerales(politicaPublicaDatosGeneralesModel);
		
	}

}
