package co.com.bogota.planeaciondistrital.service.impl;

import co.com.bogota.planeaciondistrital.model.planAccion.*;
import co.com.bogota.planeaciondistrital.repository.PlanAccionRepository;
import co.com.bogota.planeaciondistrital.service.PlanAccionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.List;

@Service
public class PlanAccionImpl extends GeneralImpl implements PlanAccionService {

    @Autowired
    private PlanAccionRepository planAccionRepository;

    private static Logger logger = LoggerFactory.getLogger(PlanAccionImpl.class);


    @Override
    public List<respuestaConsultaObjetivosModel> consultarObjetivo(String P_CODIGO_POLITICA) throws SQLException {

        return planAccionRepository.consultarObjetivo(P_CODIGO_POLITICA);
    }


    @Override
    public List<SectoresModel> ConsultarSectores() throws SQLException {

        return planAccionRepository.ConsultarSectores();
    }

    @Override
    public List<EntidadesModel> ConsultarEntidades(String P_VALOR_SECTOR) throws SQLException {

        return planAccionRepository.ConsultarEntidades(P_VALOR_SECTOR);


    }

    @Override
    public List<TipoAnualizacionModel> ConsultarTiposAnualizacion() throws SQLException {


        return planAccionRepository.ConsultarTiposAnualizacion();
    }

    @Override
    public List<UnidadesMedidasModel> ConsultarUnidadesMedidas() throws SQLException {


        return planAccionRepository.ConsultarUnidadesMedidas();

    }

    @Override
    public List<ConsultaAniosModel> ConsultarAnios(String P_CODIGO_INDICADOR) throws SQLException {


        return planAccionRepository.ConsultarAnios(P_CODIGO_INDICADOR);

    }


    @Override
    public ResultadoModel InsertarResultado(ResultadoModel modelo) throws SQLException {


        return planAccionRepository.InsertarResultado(modelo);
    }


    @Override
    public IndicadorModel InsertarIndicador(IndicadorModel modelo) throws SQLException {

        return planAccionRepository.InsertarIndicador(modelo);
    }

    @Override
    public MetaModel InsertarMeta(MetaModel modelo) throws SQLException {

        return planAccionRepository.InsertarMeta(modelo);
    }

    @Override
    public ProductoModel InsertarProducto(ProductoModel modelo) throws SQLException {

        return planAccionRepository.InsertarProducto(modelo);
    }


}
