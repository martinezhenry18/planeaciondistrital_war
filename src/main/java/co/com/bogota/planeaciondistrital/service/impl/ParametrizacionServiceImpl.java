package co.com.bogota.planeaciondistrital.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.bogota.planeaciondistrital.dto.ResponseGeneralDto;
import co.com.bogota.planeaciondistrital.exception.PlaneacionException;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListaDetalleModel;
import co.com.bogota.planeaciondistrital.model.parametrizacion.ParametrizacionListasBasicasModel;
import co.com.bogota.planeaciondistrital.repository.ParametrizacionRepository;
import co.com.bogota.planeaciondistrital.service.ParametrizacionService;

/**
	*	@author 	Henry Martinez
	*	@empresa	Ares Soluciones S.A.S
	*	@mail		hmartinez@aressoluciones.com
	*	@fecha 		20/05/2019
	*	@proyecto	Planeacion Distrital
	*	@clase 		ParametrizacionServiceImpl.java
	*	@comments	
 */

@Service
public class ParametrizacionServiceImpl implements ParametrizacionService
{
	

    private static Logger logger = LoggerFactory.getLogger(ParametrizacionServiceImpl.class);
    
    @Autowired
    private ParametrizacionRepository parametrizacionRepository;
    

    private ResponseGeneralDto objResponseGeneralDto;

	
	@Override
	public ArrayList<ParametrizacionListasBasicasModel> getListasBasicas() throws SQLException
	{
		logger.info("--PROCESO--");
		logger.info("--Obteniendo Listas Basicas--");
		ArrayList<ParametrizacionListasBasicasModel> cursor = new ArrayList<ParametrizacionListasBasicasModel>();
		
		cursor = parametrizacionRepository.getListasBasicas();
		
		return cursor;

	}

	
	@Override
	public ArrayList<ParametrizacionListaDetalleModel> getListaParametroDetalle(int idLista) throws SQLException
	{
		ArrayList<ParametrizacionListaDetalleModel> cursor = new ArrayList<ParametrizacionListaDetalleModel>();
		
		if (idLista == 55)
		{
			cursor = parametrizacionRepository.getListaParametroSemaforo(idLista);
		} else {
			cursor = parametrizacionRepository.getListaParametroDetalle(idLista);
		}
			
		
		return cursor;

	}
	
	
	@Override
	public ResponseGeneralDto guardarDetalleListaBasica(ParametrizacionListaDetalleModel modelParametrizacionListaDetalleModel) throws SQLException, PlaneacionException
	{
		
		objResponseGeneralDto = parametrizacionRepository.guardarDetalleListaBasica(modelParametrizacionListaDetalleModel);
		
		return objResponseGeneralDto;
	}
	
	@Override
	public ResponseGeneralDto actualizarDetalleListaBasica(ParametrizacionListaDetalleModel modelParametrizacionListaDetalleModel) throws SQLException, PlaneacionException
	{
		
		objResponseGeneralDto = parametrizacionRepository.actualizarDetalleListaBasica(modelParametrizacionListaDetalleModel);
		
		return objResponseGeneralDto;
	}

	@Override
	public ResponseGeneralDto actualizarParametroListaBasica(ParametrizacionListasBasicasModel modelParametrizacionListasBasicasModel) throws SQLException, PlaneacionException
	{
		
		objResponseGeneralDto = parametrizacionRepository.actualizarParametroListaBasica(modelParametrizacionListasBasicasModel);
		
		return objResponseGeneralDto;
	}


	public ResponseGeneralDto getObjResponseGeneralDto()
	{
		return objResponseGeneralDto;
	}


	public void setObjResponseGeneralDto(ResponseGeneralDto objResponseGeneralDto)
	{
		this.objResponseGeneralDto = objResponseGeneralDto;
	}

}
