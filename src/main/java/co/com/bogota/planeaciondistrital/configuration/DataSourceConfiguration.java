package co.com.bogota.planeaciondistrital.configuration;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 
 * @author Daniel Arbey Sarmiento Amaya
 *
 */
@Configuration
@EnableTransactionManagement
public class DataSourceConfiguration {

	@Value("${spring.datasource.jndi-name}")
	private String jndi;
	
    @Bean
    public DataSource getDataSource() throws IllegalArgumentException,
			NamingException {
        JndiObjectFactoryBean bean = new JndiObjectFactoryBean();           // create JNDI data source
        bean.setJndiName(jndi);  // jndiDataSource is name of JNDI data source
        bean.setProxyInterface(DataSource.class);
        bean.setLookupOnStartup(false);
        bean.afterPropertiesSet();
        return (DataSource) bean.getObject();
    }

//	@Bean
//	public DataSource dataSource() {
//		final JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
//		dsLookup.setResourceRef(true);
//		return dsLookup.getDataSource(jndi);
//	}
    
    
//	@Bean
//	public DataSource getdataSource() {
//		BasicDataSource dataSource = new BasicDataSource();
//		dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//		dataSource.setUrl("jdbc:oracle:thin:@25.4.170.106:1521:orcl");
//		dataSource.setUsername("SSEPP");
//		dataSource.setPassword("SSEPP");
//
//		return dataSource;
//	}    
    
//    @Bean
//    public LocalSessionFactoryBean sessionFactory() {
//        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
//        try {
//			sessionFactory.setDataSource(getDataSource());
//		} catch (IllegalArgumentException e) {
//			e.printStackTrace();
//		} catch (NamingException e) {
//			e.printStackTrace();
//		}
//        sessionFactory.setHibernateProperties(hibernateProperties());
// 
//        return sessionFactory;
//    }
// 
//    @Bean
//    public DataSource getdataSource() {
//        BasicDataSource dataSource = new BasicDataSource();
//        dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//        dataSource.setUrl("jdbc:oracle:thin:@25.4.170.106:1521:orcl");
//        dataSource.setUsername("SSEPP");
//        dataSource.setPassword("SSEPP");
// 
//        return dataSource;
//    }
//    
//    private final Properties hibernateProperties() {
//        Properties hibernateProperties = new Properties();
//        hibernateProperties.setProperty(
//          "hibernate.hbm2ddl.auto", "create-drop");
//        hibernateProperties.setProperty(
//          "hibernate.dialect", "org.hibernate.dialect.H2Dialect");
// 
//        return hibernateProperties;
//    }    

//    @Autowired
//    private EntityManagerFactory entityManagerFactory;
//
//    /**
//     * This bean needs to be wired in so that a SessionFactory can be wired in to other data access beans.
//     *
//     * @return a session factory bean
//     */
//	@Bean
//	public SessionFactory getSessionFactory() {
//	    if (entityManagerFactory.unwrap(SessionFactory.class) == null) {
//	        throw new NullPointerException("factory is not a hibernate factory");
//	    }
//	    return entityManagerFactory.unwrap(SessionFactory.class);
//	}
}
